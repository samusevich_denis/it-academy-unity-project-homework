﻿using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;

public class TetrisController : MonoBehaviour
{
    private static int m_widthField = 12;
    private static int m_heightField = 20;
    GameObject[] bordersBottom = new GameObject[m_widthField];
    GameObject[] bordersLeft = new GameObject[m_heightField];
    GameObject[] bordersRight = new GameObject[m_heightField];
    private GameFigure gameСenterFigure;
    private GameFigure gameNextFigure;
    private List<Coordinate> coordinatesStopFigure = new List<Coordinate>();
    private List<GameObject> gameObjectsAllBottom = new List<GameObject>();
    private static Vector3 positionFigureMove = new Vector3((float)(m_widthField * 0.5), (float)(m_heightField), 0.0f);
    private static Vector3 positionNextFigure = new Vector3((float)(m_widthField*1.4), (float)(m_heightField), 0.0f);
    private int counterFigures = 0;
    // Start is called before the first frame update
    void Start()
    {
        CreateLine(bordersBottom, 0.0f, 0.0f, EDirection.horizontal);
        CreateLine(bordersLeft, 12.0f, 0.0f, EDirection.vertical);
        CreateLine(bordersRight, -1.0f, 0.0f, EDirection.vertical);
        for (int i = 0; i < bordersBottom.Length; i++)
        {
            coordinatesStopFigure.Add(new Coordinate((int)bordersBottom[i].transform.position.x, (int)(bordersBottom[i].transform.position.y)));
        }
        gameСenterFigure = GameFigure.CreateFigure(positionFigureMove);
        gameNextFigure = GameFigure.CreateFigure(positionNextFigure);
    }

    // Update is called once per frame
    void Update()
    {
        Thread.Sleep(1000);
        if (gameСenterFigure.StopMove(coordinatesStopFigure))
        {
            for (int i = 0; i < gameСenterFigure.GameObjectsFigure.Length; i++)
            {
                coordinatesStopFigure.Add(new Coordinate((int)gameСenterFigure.GameObjectsFigure[i].transform.position.x, (int)(gameСenterFigure.GameObjectsFigure[i].transform.position.y)));
                gameObjectsAllBottom.Add(gameСenterFigure.GameObjectsFigure[i]);
            }
            gameСenterFigure = gameNextFigure;
            gameСenterFigure.SetPositionFigureMove();
            gameNextFigure = GameFigure.CreateFigure(positionNextFigure);
        }
        else
        {
            gameСenterFigure.MoveDown();
        }
    }

    private enum EDirection
    {
        horizontal,
        vertical,
    }

    private void CreateLine(GameObject[] gameObjects, float startCoordinateX, float startCoordinateY, EDirection direction)
    {
        if (direction == EDirection.horizontal)
        {
            for (int i = 0; i < gameObjects.Length; i++)
            {
                gameObjects[i] = GameObject.CreatePrimitive(PrimitiveType.Cube);
                gameObjects[i].transform.position = new Vector3(startCoordinateX + i, startCoordinateY, 0);
            }
        }
        else if (direction == EDirection.vertical)
        {
            for (int i = 0; i < gameObjects.Length; i++)
            {
                gameObjects[i] = GameObject.CreatePrimitive(PrimitiveType.Cube);
                gameObjects[i].transform.position = new Vector3(startCoordinateX, startCoordinateY + i, 0);
            }
        }
    }

    struct Coordinate
    {
        internal int CoordinateX { get; set; }
        internal int CoordinateY { get; set; }
        internal Coordinate(int coordinateX, int coordinateY)
        {
            CoordinateX = coordinateX;
            CoordinateY = coordinateY;
        }
    }

    class GameFigure
    {
        internal GameObject[] GameObjectsFigure { get; } = new GameObject[4];
        private static System.Random random = new System.Random();
        internal Coordinate[] CoordinatesBottom { get; }
        private GameFigure(int number, Vector3 positionFigure)
        {
            switch (number)
            {
                case 0:
                    {
                        for (int i = 0; i < GameObjectsFigure.Length; i++)
                        {
                            GameObjectsFigure[i] = GameObject.CreatePrimitive(PrimitiveType.Cube);
                        }
                        GameObjectsFigure[0].transform.position = positionFigure;
                        GameObjectsFigure[1].transform.position = new Vector3(positionFigure.x + 1, positionFigure.y, 0);
                        GameObjectsFigure[2].transform.position = new Vector3(positionFigure.x, positionFigure.y - 1, 0);
                        GameObjectsFigure[3].transform.position = new Vector3(positionFigure.x + 1, positionFigure.y - 1, 0);
                        CoordinatesBottom = new Coordinate[2] {new Coordinate((int)positionFigure.x, (int)positionFigure.y -2),
                            new Coordinate((int)positionFigure.x+1, (int)(positionFigure.y -2)) };
                        break;
                    }
                case 1:
                    {
                        for (int i = 0; i < GameObjectsFigure.Length; i++)
                        {
                            GameObjectsFigure[i] = GameObject.CreatePrimitive(PrimitiveType.Cube);
                        }
                        GameObjectsFigure[0].transform.position = positionFigure;
                        GameObjectsFigure[1].transform.position = new Vector3(positionFigure.x + 1, positionFigure.y, 0);
                        GameObjectsFigure[2].transform.position = new Vector3(positionFigure.x, positionFigure.y - 1, 0);
                        GameObjectsFigure[3].transform.position = new Vector3(positionFigure.x, positionFigure.y - 2, 0);
                        CoordinatesBottom = new Coordinate[2] {new Coordinate((int)positionFigure.x, (int)positionFigure.y -3),
                            new Coordinate((int)positionFigure.x+1, (int)positionFigure.y -1) };
                        break;
                    }
                case 2:
                    {
                        for (int i = 0; i < GameObjectsFigure.Length; i++)
                        {
                            GameObjectsFigure[i] = GameObject.CreatePrimitive(PrimitiveType.Cube);
                        }
                        GameObjectsFigure[0].transform.position = positionFigure;
                        GameObjectsFigure[1].transform.position = new Vector3(positionFigure.x + 1, positionFigure.y, 0);
                        GameObjectsFigure[2].transform.position = new Vector3(positionFigure.x, positionFigure.y - 1, 0);
                        GameObjectsFigure[3].transform.position = new Vector3(positionFigure.x - 1, positionFigure.y - 1, 0);
                        CoordinatesBottom = new Coordinate[3] {new Coordinate((int)positionFigure.x+1, (int)positionFigure.y -1),
                            new Coordinate((int)positionFigure.x, (int)positionFigure.y -2),
                            new Coordinate((int)positionFigure.x-1, (int)positionFigure.y -2)};
                        break;
                    }
                case 3:
                    {
                        for (int i = 0; i < GameObjectsFigure.Length; i++)
                        {
                            GameObjectsFigure[i] = GameObject.CreatePrimitive(PrimitiveType.Cube);
                        }
                        GameObjectsFigure[0].transform.position = positionFigure;
                        GameObjectsFigure[1].transform.position = new Vector3(positionFigure.x + 1, positionFigure.y - 1, 0);
                        GameObjectsFigure[2].transform.position = new Vector3(positionFigure.x, positionFigure.y - 1, 0);
                        GameObjectsFigure[3].transform.position = new Vector3(positionFigure.x - 1, positionFigure.y - 1, 0);
                        CoordinatesBottom = new Coordinate[3] {new Coordinate((int)positionFigure.x+1, (int)positionFigure.y -2),
                            new Coordinate((int)positionFigure.x, (int)positionFigure.y -2),
                            new Coordinate((int)positionFigure.x-1, (int)positionFigure.y -2)};
                        break;
                    }
                case 4:
                    {
                        for (int i = 0; i < GameObjectsFigure.Length; i++)
                        {
                            GameObjectsFigure[i] = GameObject.CreatePrimitive(PrimitiveType.Cube);
                        }
                        GameObjectsFigure[0].transform.position = positionFigure;
                        GameObjectsFigure[1].transform.position = new Vector3(positionFigure.x - 1, positionFigure.y, 0);
                        GameObjectsFigure[2].transform.position = new Vector3(positionFigure.x + 1, positionFigure.y, 0);
                        GameObjectsFigure[3].transform.position = new Vector3(positionFigure.x + 2, positionFigure.y, 0);
                        CoordinatesBottom = new Coordinate[4] {new Coordinate((int)positionFigure.x-1, (int)positionFigure.y -1),
                            new Coordinate((int)positionFigure.x, (int)positionFigure.y -1),
                            new Coordinate((int)positionFigure.x-1, (int)positionFigure.y -1),
                            new Coordinate((int)positionFigure.x-2, (int)positionFigure.y -1)};
                        break;
                    }
                case 5:
                    {
                        for (int i = 0; i < GameObjectsFigure.Length; i++)
                        {
                            GameObjectsFigure[i] = GameObject.CreatePrimitive(PrimitiveType.Cube);
                        }
                        GameObjectsFigure[0].transform.position = positionFigure;
                        GameObjectsFigure[1].transform.position = new Vector3(positionFigure.x, positionFigure.y - 1, 0);
                        GameObjectsFigure[2].transform.position = new Vector3(positionFigure.x + 1, positionFigure.y - 1, 0);
                        GameObjectsFigure[3].transform.position = new Vector3(positionFigure.x + 2, positionFigure.y - 1, 0);
                        CoordinatesBottom = new Coordinate[3] {new Coordinate((int)positionFigure.x, (int)positionFigure.y -2),
                            new Coordinate((int)positionFigure.x-1, (int)positionFigure.y -2),
                            new Coordinate((int)positionFigure.x-2, (int)positionFigure.y -2)};

                        break;
                    }
            }

        }

        internal static GameFigure CreateFigure(Vector3 positionFigure)
        {
            return new GameFigure(random.Next(0, 5), positionFigure);
        }

        internal void SetPositionFigureMove()
        {
            for (int i = 0; i < CoordinatesBottom.Length; i++)
            {
                CoordinatesBottom[i].CoordinateX = CoordinatesBottom[i].CoordinateX - (int)(positionNextFigure.x - positionFigureMove.x);
            }
            for (int i = 0; i < GameObjectsFigure.Length; i++)
            {
                GameObjectsFigure[i].transform.position =
                    new Vector3(GameObjectsFigure[i].transform.position.x - (positionNextFigure.x - positionFigureMove.x),
                    GameObjectsFigure[i].transform.position.y, GameObjectsFigure[i].transform.position.z);
            }
        }

        internal void MoveDown()
        {
            for (int i = 0; i < CoordinatesBottom.Length; i++)
            {
                CoordinatesBottom[i].CoordinateY = CoordinatesBottom[i].CoordinateY - 1;
            }
            for (int i = 0; i < GameObjectsFigure.Length; i++)
            {
                GameObjectsFigure[i].transform.position = new Vector3(GameObjectsFigure[i].transform.position.x,
                    GameObjectsFigure[i].transform.position.y - 1, GameObjectsFigure[i].transform.position.z);
            }
        }
        
        internal bool StopMove(List<Coordinate> coordinatesStop)
        {
            for (int i = 0; i < coordinatesStop.Count; i++)
            {
                for (int j = 0; j < CoordinatesBottom.Length; j++)
                {
                    if (coordinatesStop[i].CoordinateX == CoordinatesBottom[j].CoordinateX && coordinatesStop[i].CoordinateY == CoordinatesBottom[j].CoordinateY)
                    {
                        return true;
                    }
                }
            }
            return false;
        }
    }
}
