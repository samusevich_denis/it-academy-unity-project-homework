﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class HopPlayer : MonoBehaviour
{

    [SerializeField] private AnimationCurve m_baseJumpCurve;
    [SerializeField] private AnimationCurve m_redJumpCurve;
    [SerializeField] private AnimationCurve m_blueJumpCurve;
    [SerializeField] private AnimationCurve m_purpleJumpCurve;
    [SerializeField] private float m_JumpHeight = 1f;
    [SerializeField] private float m_JumpDistance = 2f;
    [SerializeField] private float m_BallSpeed = 1f;
    [SerializeField] private HopInput m_Input;
    [SerializeField] private HopTrack m_Track;
    private AnimationCurve jumpCurve;
    private float iteration; //цикл прыжка

    private float startZ; // точка начала прыжка

    private void GetParametrsJump(EHopColorPlatform colorPlatform)
    {
        switch (colorPlatform)
        {
            case EHopColorPlatform.blueView:
            {
                m_JumpHeight = 2f;
                m_JumpDistance = 2f;
                m_BallSpeed = 1.5f;
                jumpCurve = m_blueJumpCurve;
                break;
            }
            case EHopColorPlatform.redView:
            {
                m_JumpHeight = 2f;
                m_JumpDistance = 4f;
                m_BallSpeed = 1f;
                jumpCurve = m_redJumpCurve;
                break;
            }
            case EHopColorPlatform.purpleView:
            {
                m_JumpHeight = 1f;
                m_JumpDistance = 2f;
                m_BallSpeed = 0.5f;
                jumpCurve = m_purpleJumpCurve;
                break;
            }
            default:
            {
                m_JumpHeight = 1f;
                m_JumpDistance = 2f;
                m_BallSpeed = 1f;
                jumpCurve = m_baseJumpCurve;
                break;
            }
        }
    }
        // Start is called before the first frame update
    void Start()
    {
        GetParametrsJump(EHopColorPlatform.baseView);
    }
    // Update is called once per frame
    void Update()
    {
        var pos = transform.position;
        pos.x = Mathf.Lerp(pos.x, m_Input.Strafe, Time.deltaTime * 5f);
        // прыжок
        pos.y = jumpCurve.Evaluate(iteration)*m_JumpHeight;
        
        //вижение вперед
        pos.z = startZ + iteration * m_JumpDistance;
        transform.position = pos;

        iteration += Time.deltaTime * m_BallSpeed;
        if (iteration < 1f)
        {
            return;
        }

        iteration = 0;
        startZ += m_JumpDistance;
        var colorPlatform = EHopColorPlatform.baseView;
        if (m_Track.IsBallOnPlatform(transform.position, out colorPlatform))
        {
            GetParametrsJump(colorPlatform);
            return;
        }

        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
}
