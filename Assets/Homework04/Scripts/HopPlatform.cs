﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HopPlatform : MonoBehaviour
{
    [SerializeField] private GameObject m_baseView;
    [SerializeField] private GameObject m_greenView;
    [SerializeField] private GameObject m_redView;
    [SerializeField] private GameObject m_blueView;
    [SerializeField] private GameObject m_purpleView;

    public EHopColorPlatform GetColorPlatform()
    {
        EHopColorPlatform colorView = EHopColorPlatform.noneView;
        if (m_baseView.activeSelf)
        {
            colorView = EHopColorPlatform.baseView;
        }
        else if (m_greenView.activeSelf)
        {
            colorView = EHopColorPlatform.greenView;
        }
        else if (m_redView.activeSelf)
        {
            colorView = EHopColorPlatform.redView;
        }
        else if (m_blueView.activeSelf)
        {
            colorView = EHopColorPlatform.blueView;
        }
        else if (m_purpleView.activeSelf)
        {
            colorView = EHopColorPlatform.purpleView;
        }
        return colorView;
    }
    public void SetGreen()
    {
        SetColorPlatform(EHopColorPlatform.greenView);
        Invoke(nameof(SetBase),0.5f);
    }
    private void SetBase()
    {
        SetColorPlatform(EHopColorPlatform.baseView);
    }

    private void SetPlatformActiveFalse()
    {
        m_baseView.SetActive(false);
        m_redView.SetActive(false);
        m_blueView.SetActive(false);
        m_purpleView.SetActive(false);
        m_greenView.SetActive(false);
    }
    private void SetColorPlatform(EHopColorPlatform ColorVeiw)
    {
        SetPlatformActiveFalse();
        switch (ColorVeiw)
        {
            case EHopColorPlatform.noneView:
            {
                break;
            }
            case EHopColorPlatform.baseView:
            {
                m_baseView.SetActive(true);
                break;
            }
            case EHopColorPlatform.greenView:
            {
                m_greenView.SetActive(true);
                break;
            }
            case EHopColorPlatform.redView:
            {
                m_redView.SetActive(true);
                break;
            }
            case EHopColorPlatform.blueView:
            {
                m_blueView.SetActive(true);
                break;
            }
            case EHopColorPlatform.purpleView:
            {
                m_purpleView.SetActive(true);
                break;
            }
        }
    }

    private void SetRandomPlatformColor()
    {
        switch (Random.Range(0,5))
        {
            case 0:
            {
                SetColorPlatform(EHopColorPlatform.blueView);
                break;
            }
            case 1:
            {
                SetColorPlatform(EHopColorPlatform.redView);
                break;
            }
            case 2:
            {
                SetColorPlatform(EHopColorPlatform.purpleView);
                break;
            }
            default:
            {
                SetColorPlatform(EHopColorPlatform.baseView);
                break;
            }
        }
    }
    
    // Start is called before the first frame update
    void Start()
    {
        SetRandomPlatformColor();
    }
}
