﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HopCachNearbyPlatform 
{
    public int IndexCurrentPlatform {get; private set;}
    public List<int> indexNearlyPlatforms;
    public HopCachNearbyPlatform(int indexCurrentPlatform)
    {
        IndexCurrentPlatform=indexCurrentPlatform;
        indexNearlyPlatforms = new List<int>();
    }

}
