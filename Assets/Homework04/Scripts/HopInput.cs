﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HopInput : MonoBehaviour
{

    private float strafe;
    public float Strafe => strafe;

    private float screenCenter;
    

    // Start is called before the first frame update
    private void Start()
    {
        screenCenter = Screen.width * 0.5f;
    }

    // Update is called once per frame
    private void Update()
    {
        if (!Input.GetMouseButton(0))
        {
            return;
        }

        var mousePositions = Input.mousePosition.x;
        if (mousePositions > screenCenter)
        {
            strafe = 3*(mousePositions - screenCenter) / screenCenter;
        }
        else
        {
            strafe = 1f - mousePositions / screenCenter;
            strafe *= -3f;
        }
    }
}
