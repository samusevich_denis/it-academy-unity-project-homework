﻿using System.Collections.Generic;
using UnityEngine;

public class HopTrack : MonoBehaviour
{
    [SerializeField] private GameObject m_Platform;
    [SerializeField] private bool m_useRandomSeed;
    [SerializeField] private int m_seed = 123456;
    private List<GameObject> platforms = new List<GameObject>();
    private HopCachNearbyPlatform[] nearbyPlatformsCach;
    private int currentPlatform;

    // Start is called before the first frame update
    private void Start()
    {
        platforms.Add(m_Platform);

        if (m_useRandomSeed)
        {
            Random.InitState(m_seed);
        }
        
        for (int i = 0; i < 50; i++)
        {
            GameObject obj = Instantiate(m_Platform, transform);
            Vector3 pos = Vector3.zero;

            pos.z = 2 * (i + 1);
            int randomPos = Random.Range(-3, 4);
            pos.x = Random.Range(-3, 4);
            obj.transform.position = pos;

            obj.name = $"Platform_{i + 1}";
            platforms.Add(obj);
            if (Random.Range(0,2)==1)
            {   
                Vector3 newPos = Vector3.zero;
                int newRandomPos = Random.Range(-3, 4);
                while (randomPos==newRandomPos)
                {
                    newRandomPos = Random.Range(-3, 4);
                }
                GameObject newObj = Instantiate(m_Platform, transform);
                newPos.z = pos.z;
                newPos.x = newRandomPos;
                newObj.transform.position = newPos;

                newObj.name = $"Platform_{i + 1}.1";
                platforms.Add(newObj);
            }
        }
        nearbyPlatformsCach = new HopCachNearbyPlatform[platforms.Count];
        for (int i = 0; i < nearbyPlatformsCach.Length; i++)
        {
            nearbyPlatformsCach[i] = new HopCachNearbyPlatform(i);
            for (int j = 0; j < platforms.Count; j++)
            {
                if (nearbyPlatformsCach[i].IndexCurrentPlatform == j)
                {
                    continue;
                }
                if (platforms[j].transform.position.z < platforms[nearbyPlatformsCach[i].IndexCurrentPlatform].transform.position.z )
                {
                    continue;
                }
                if (platforms[j].transform.position.z > platforms[nearbyPlatformsCach[i].IndexCurrentPlatform].transform.position.z+5 )
                {
                    continue;
                }
                nearbyPlatformsCach[i].indexNearlyPlatforms.Add(j);
            }
        }
        for (int i = 0; i < nearbyPlatformsCach.Length; i++)
        {
            bool isIndexfirstPlatform = true;
            for (int j = 0; j < nearbyPlatformsCach.Length; j++)
            {
                for (int k = 0; k < nearbyPlatformsCach[j].indexNearlyPlatforms.Count; k++)
                {
                    if(nearbyPlatformsCach[i].IndexCurrentPlatform==nearbyPlatformsCach[j].indexNearlyPlatforms[k])
                    {
                        isIndexfirstPlatform = false;
                    }
                }
            }
            if (isIndexfirstPlatform)
            {
                currentPlatform = i;
                break;
            }
        }
    }

    public HopCachNearbyPlatform GetNearbyPlatform()
    {   
        HopCachNearbyPlatform nearbyPlatform = new HopCachNearbyPlatform(currentPlatform);
        for (int i = 0; i < nearbyPlatformsCach.Length; i++)
        {
            if (nearbyPlatformsCach[i].IndexCurrentPlatform == currentPlatform)
            {
                nearbyPlatform = nearbyPlatformsCach[i];
            }
        }
        return nearbyPlatform;
    }
    public bool IsBallOnPlatform(Vector3 position, out EHopColorPlatform colorView)
    {
        position.y = 0f;

        var nearbyPlatforms = GetNearbyPlatform(); 
        
        for (int i = 0; i < nearbyPlatforms.indexNearlyPlatforms.Count; i++)
        {
            var platformZ = platforms[nearbyPlatforms.indexNearlyPlatforms[i]].transform.position.z;
            var platformX = platforms[nearbyPlatforms.indexNearlyPlatforms[i]].transform.position.x;
            
            if (platformZ + 0.5f > position.z&&platformZ - 0.5f<position.z)
            {
                if (platformX+0.5f> position.x&& platformX-0.5f<position.x)
                {
                    
                    var platform = platforms[nearbyPlatforms.indexNearlyPlatforms[i]].GetComponent<HopPlatform>();
                    colorView = platform.GetColorPlatform();
                    platform.SetGreen();
                    currentPlatform = nearbyPlatforms.indexNearlyPlatforms[i];
                    return true;
                }
                
            }
        }
        colorView = EHopColorPlatform.baseView;
        return false;
    }
}
