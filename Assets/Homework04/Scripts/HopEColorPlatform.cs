﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum EHopColorPlatform
{
    noneView = 0,
    baseView = 1,
    greenView = 2,
    redView = 3,
    blueView = 4,
    purpleView = 5,
}
