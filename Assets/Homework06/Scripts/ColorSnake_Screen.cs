﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ColorSnake_Screen : MonoBehaviour
{
    [SerializeField] private GameObject m_GameObjectData;
    [SerializeField] private GameObject m_ResultText;
    [SerializeField] private GameObject m_ResultCheckpoint;
    [SerializeField] private GameObject m_ResultPoints;
    // Start is called before the first frame update
    void Start()
    {
        m_GameObjectData = GameObject.Find("GameData");
        var dataGame = m_GameObjectData.GetComponent<ColorSnake_DataGame>();
        if (dataGame.GameOver)
        {
            var resultText = m_ResultText.GetComponent<Text>();
            resultText.text = "Game Over";
            var resultCheckpoint = m_ResultCheckpoint.GetComponent<Text>();
            resultCheckpoint.text = $"You passed\n{dataGame.CountCheckPoint}\ncheckpoints";
            var resultPoints = m_ResultPoints.GetComponent<Text>();
            resultPoints.text = $"and earned\n{dataGame.CountPoint * 10 + dataGame.CountCheckPoint * 15}\npoints";
            dataGame.ResetGame();
        }
        else
        {
            var resultText = m_ResultText.GetComponent<Text>();
            resultText.text = $"Level {dataGame.Level}";
            var resultCheckpoint = m_ResultCheckpoint.GetComponent<Text>();
            resultCheckpoint.text = $"You passed\n{dataGame.CountCheckPoint}\ncheckpoints";
            var resultPoints = m_ResultPoints.GetComponent<Text>();
            resultPoints.text = $"and earned\n{dataGame.CountPoint * 10 + dataGame.CountCheckPoint * 15}\npoints";
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.anyKeyDown)
        {
            SceneManager.LoadScene(0);
        }
    }
}
