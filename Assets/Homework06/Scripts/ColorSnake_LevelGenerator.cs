﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorSnake_LevelGenerator : MonoBehaviour
{
    [SerializeField] private ColorSnake_Types m_Types;
    [SerializeField] private ColorSnake_GameController m_Controller;
    private int line = 1;//номер генерируей линии препятствий
    private List<GameObject> obstaclesAndCheckPoint = new List<GameObject>();
    private int countCheckPoint;
    // Start is called before the first frame update
    void Start()
    {
        var upBorder = m_Controller.Bounds.Up;
        GenerateObstaclesAndCheckPoint(0);
        while (line * 2f < upBorder + 2f)
        {
            GenerateObstaclesAndCheckPoint(1);
        }
    }

    void Update()
    {
        var upBorder = m_Controller.Bounds.Up + m_Controller.MainCamera.transform.position.y;
        if (countCheckPoint < m_Controller.CountCheckPointInLevel)
        {
            if (line * 2f > upBorder + 2f)
                return;
            if (line % 8 == 0)
            {
                GenerateObstaclesAndCheckPoint(0);
            }
            else
            {
                GenerateObstaclesAndCheckPoint(1);
            }
            var downBorder = m_Controller.MainCamera.transform.position.y + m_Controller.Bounds.Down;
            if (obstaclesAndCheckPoint[0].transform.position.y + 8f < downBorder)
            {
                Destroy(obstaclesAndCheckPoint[0]);
                obstaclesAndCheckPoint.RemoveAt(0);
            }
        }
    }
    private void GenerateObstaclesAndCheckPoint(int id)
    {
        TemplateType template;
        GameObject obstacle;
        ObjectType objType;
        DynamicsTypes randomDynamicsType;
        var randomMove = Random.Range(0, 2);
        if (id == 0)
        {
            countCheckPoint++;
            template = m_Types.GetTemplateType(0);
            obstacle = new GameObject("Checkpoint_" + line.ToString());
            objType = m_Types.GetObjectType(0);
            randomDynamicsType = m_Types.GetDynamicsType(0);
            randomMove = 1;
        }
        else
        {
            template = m_Types.GetRandomTemplateType();
            obstacle = new GameObject("Obstacle_" + line.ToString());
            objType = m_Types.GetRandomObjectType();
            randomDynamicsType = m_Types.GetRandomDynamicsType();
            if (randomMove == 0)
            {
                randomMove = 1;
            }
            else
            {
                randomMove = -1;
            }
        }
        foreach (var point in template.Points)
        {
            var colorType = m_Types.GetRandomColorType();
            var obj = Instantiate(objType.Object, point.transform.position, point.transform.rotation);
            obj.transform.parent = obstacle.transform;
            obj.GetComponent<SpriteRenderer>().color = colorType.Color;
            var obstacleController = obj.AddComponent<ColorSnake_Obstacle>();
            obstacleController.DynamicsTypes = randomDynamicsType;
            obstacleController.RandomMove = randomMove;
            obstacleController.ColorId = colorType.Id;
            obstacleController.TransformParent = obstacle.transform;
            if (id == 0)
            {
                obstacleController.IsCheckpoint = true;
            }
            else
            {
                obstacleController.IsCheckpoint = false;
            }
        }

        Vector3 pos = obstacle.transform.position;
        pos.y = line * 4f;//2 - расстояние между препятсвиями
        obstacle.transform.position = pos;
        line++;
        obstaclesAndCheckPoint.Add(obstacle);
    }
}

