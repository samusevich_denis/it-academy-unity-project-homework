﻿using System;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

//цвета объектов
[Serializable]
public class ColorType
{
    public string Name;
    public int Id;
    public Color Color;
}
    
//Типы объектов
[Serializable]
public class ObjectType
{
    public string Name;
    public int Id;
    public GameObject Object;
}

//шаблоны 
[Serializable]
public class TemplateType
{
    public string Name;
    public int Id;
    public Transform[] Points;
}

[Serializable]
public class DynamicsTypes
{
    public string Name;
    public int Id;
}
public class ColorSnake_Types : MonoBehaviour
{
    [SerializeField] private ColorType[] m_Colors;
    [SerializeField] private ObjectType[] m_Objects;
    [SerializeField] private TemplateType[] m_Templates;
    [SerializeField] private DynamicsTypes[] m_DynamicsTypes;

    public ColorType GetRandomColorType()
    {
        int rand = Random.Range(0, m_Colors.Length);
        return m_Colors[rand];
    }
    public ObjectType GetRandomObjectType()
    {
        int rand = Random.Range(1, m_Objects.Length);
        return m_Objects[rand];
    }
    public TemplateType GetRandomTemplateType()
    {
        int rand = Random.Range(1, m_Templates.Length);
        return m_Templates[rand];
    }
    public DynamicsTypes GetRandomDynamicsType()
    {
        int rand = Random.Range(0, m_DynamicsTypes.Length);
        return m_DynamicsTypes[rand];
    }
    public ColorType GetColorType(int id)
    {
        return m_Colors.FirstOrDefault(c => c.Id == id);
    }
    public ObjectType GetObjectType(int id)
    {
        return m_Objects.FirstOrDefault(o => o.Id == id);
    }
    public TemplateType GetTemplateType(int id)
    {
        return m_Templates.FirstOrDefault(t => t.Id == id);
    }
    public DynamicsTypes GetDynamicsType(int id)
    {
        return m_DynamicsTypes.FirstOrDefault(d => d.Id == id);
    }
}
