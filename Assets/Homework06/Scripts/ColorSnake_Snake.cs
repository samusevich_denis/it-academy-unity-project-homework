﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ColorSnake_Snake : MonoBehaviour
{
    [SerializeField] private ColorSnake_GameController m_GameController;
    [SerializeField] private SpriteRenderer m_SpriteRenderer;
    private ColorSnake_DataGame dataGame;
    private Vector3 positionLastSnake;
    private Vector3 position;
    private Vector3 positionMove;
    private Vector3 positionMouseButtonDown;
    public int CurrentColorId { get; private set; }

    private void Start()
    {
        GameObject dataGameObject = GameObject.Find("GameData");
        if (dataGameObject!=null)
        {
            dataGame = dataGameObject.GetComponent<ColorSnake_DataGame>();
        }
        else
        {
            dataGameObject = new GameObject("GameData");
            dataGame = dataGameObject.AddComponent<ColorSnake_DataGame>();
            DontDestroyOnLoad(dataGameObject);
        }
        position = transform.position;
        var colorType = m_GameController.Types.GetRandomColorType();
        CurrentColorId = colorType.Id;
        m_SpriteRenderer.color = colorType.Color;
    }

    private void SetColor(int id)
    {
        var colorType = m_GameController.Types.GetColorType(id);
        CurrentColorId = colorType.Id;
        m_SpriteRenderer.color = colorType.Color;
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        var obstacle = other.gameObject.GetComponent<ColorSnake_Obstacle>();
        if (obstacle.IsCheckpoint)
        {
            dataGame.AddPointCheckPoint();
            if (dataGame.CountCheckPoint % m_GameController.CountCheckPointInLevel == 0)
            {
                dataGame.NextLevel();
            }
            SetColor(obstacle.ColorId);
            Destroy(obstacle.gameObject);
        }
        if (obstacle.ColorId == CurrentColorId)
        {
            dataGame.AddPoint();
            Destroy(obstacle.gameObject);
        }
        else
        {
            dataGame.GameOverPlayer();
        }
    }

    // Update is called once per frame
    void Update()
    {
        position = transform.position;
        if (!Input.GetMouseButton(0))
        {
            return;
        }
        if (Input.GetMouseButtonDown(0))
        {
            positionMouseButtonDown.x = m_GameController.MainCamera.ScreenToWorldPoint(Input.mousePosition).x;
            positionLastSnake = position;
        }

        positionMove.x = m_GameController.MainCamera.ScreenToWorldPoint(Input.mousePosition).x;
        var offsetPosition = positionMove.x - positionMouseButtonDown.x;
        float min = m_GameController.Bounds.Left;
        float max = m_GameController.Bounds.Right;

        position.x = Mathf.Clamp(positionLastSnake.x + offsetPosition, min, max);
        transform.position = position;
    }
}
