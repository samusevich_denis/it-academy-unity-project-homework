﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorSnake_Obstacle : MonoBehaviour
{
    [HideInInspector] public int ColorId;
    [HideInInspector] public bool IsCheckpoint;
    [HideInInspector] public DynamicsTypes DynamicsTypes;
    [HideInInspector] public Transform TransformParent;
    [HideInInspector] public int RandomMove;

    // Update is called once per frame
    void Update()
    {
        if (DynamicsTypes.Id == 0)
        {
            return;
        }
        if (DynamicsTypes.Id == 1)
        {

            float rotate = Time.deltaTime * 20* RandomMove;
            transform.RotateAround(TransformParent.position, Vector3.forward, rotate);
        }
    }
}
