﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorSnake_SnakeTail : MonoBehaviour
{
    [SerializeField] ColorSnake_Snake m_Snake;
    [SerializeField] LineRenderer m_lineRenderer;
    [SerializeField] ColorSnake_Types m_Types;
    private int colorId;
    private Vector3 previousPosition;
    // Start is called before the first frame update
    void Start()
    {
        Vector3[] corners = new Vector3[transform.childCount];
        for (int i = 0; i < corners.Length; i++)
        {
            GameObject obj = transform.GetChild(i).gameObject;
            corners[i] = obj.transform.position;
        }
        m_lineRenderer.positionCount = corners.Length;
        m_lineRenderer.SetPositions(corners);
        colorId = m_Types.GetColorType(m_Snake.CurrentColorId).Id;
        m_lineRenderer.SetColors(m_Types.GetColorType(colorId).Color, m_Types.GetColorType(colorId).Color);
        previousPosition = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        if(transform.position.y - previousPosition.y < 0.05f)
        {
            return;
        }
        Vector3 pos;
        if (colorId!= m_Snake.CurrentColorId) 
        {
            colorId = m_Snake.CurrentColorId;
            m_lineRenderer.SetColors(m_Types.GetColorType(colorId).Color, m_Types.GetColorType(colorId).Color);
        }
        for (int i = m_lineRenderer.positionCount-1; i >0; i--)
        {
            pos = m_lineRenderer.GetPosition(i - 1);
            pos.y -=0.05f;
            m_lineRenderer.SetPosition(i, pos);
        }
        pos = m_Snake.transform.position;

        m_lineRenderer.SetPosition(0, pos);
        previousPosition = transform.position;
    }
}
