﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ColorSnake_DataGame : MonoBehaviour
{
    public int CountCheckPoint { get; private set; }
    public int CountPoint { get; private set; }
    public int Level { get; private set; } = 1;
    public bool GameOver { get; private set; }

    public void ResetGame()
    {
        CountCheckPoint = 0;
        CountPoint = 0;
        Level = 1;
        GameOver = false;
    }
    public void NextLevel()
    {
        Level++;
        SceneManager.LoadScene(1);
    }
    public void GameOverPlayer()
    {
        GameOver = true;
        SceneManager.LoadScene(1);
    }
    public void AddPointCheckPoint()
    {
        CountCheckPoint++;
    }
    public int GetCountCheckPoint()
    {
        return CountCheckPoint;
    }
    public void AddPoint()
    {
        CountPoint++;
    }
}
