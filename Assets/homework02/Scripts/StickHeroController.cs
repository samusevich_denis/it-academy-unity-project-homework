﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;

public class StickHeroController : MonoBehaviour
{
    [SerializeField] private StickHeroStick m_Stick;
    [SerializeField] private StickHeroPlayer m_Player;
    [SerializeField] private StickHeroPlatform[] m_Platforms;

    private int counter; // Счетчик платформ
    private int points;
    private enum EGameState
    {
        Wait,
        Scaling,
        Rotate,
        Movement,
        Defeat,
    }

    private EGameState currentGameState;

    // Start is called before the first frame update
    private void Start()
    {
        currentGameState = EGameState.Wait;
        counter = 0;
        points = 0;
        m_Stick.ResetStick(m_Platforms[0].GetStickPosition());
    }

    // Update is called once per frame
    private void Update()
    {
        if (!Input.GetMouseButtonDown(0))
        {
            return;
        }

        switch (currentGameState)
        {
            case EGameState.Wait:
                currentGameState = EGameState.Scaling;
                m_Stick.StartScaling();
                break;

            case EGameState.Scaling:
                currentGameState = EGameState.Rotate;
                m_Stick.StopScaling();
                break;

            case EGameState.Rotate:
            case EGameState.Movement:
                break;

            case EGameState.Defeat:
                print("Game restarted");
                SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
                break;

            default:
                throw new ArgumentOutOfRangeException();
        }

    }

    public void StopStickScale()
    {
        currentGameState = EGameState.Rotate;
        m_Stick.StartRotate();
    }
    //TODO Выкинуть?
    public void StopStickRotate()
    {
        currentGameState = EGameState.Movement;
    }

    public void StartPlayerMovement(float length)
    {

        currentGameState = EGameState.Movement;
        StickHeroPlatform nextPlatform = m_Platforms[counter == 4 ? 0 : counter + 1];

        //находим минимальную длину стика для успешного перехода
        float targetLength = nextPlatform.transform.position.x - m_Stick.transform.position.x;
        float platformSize = nextPlatform.GetPlatformSize();
        float min = targetLength - platformSize * 0.5f;
        min -= m_Player.transform.localScale.x;

        //находим максимальную длину стика
        float max = targetLength + platformSize * 0.5f;

        //при успехе игрок переходит в центр платформы иначе падаем
        if (length>min && length < max)
        {
            m_Player.StartMovement(nextPlatform.transform.position.x, false);
            points = points+10;
        }
        else
        {
            float targetPosition = m_Stick.transform.position.x + length + m_Player.transform.localScale.x*0.6f;
            m_Player.StartMovement(targetPosition, true);
        }
    }

    public void StopPlayerMovement()
    {
        currentGameState = EGameState.Wait;
        counter= counter == 4 ? 0 : counter+1;
        m_Stick.ResetStick(m_Platforms[counter].GetStickPosition());
    }

    public void ShowScores()
    {
        currentGameState = EGameState.Defeat;
        print($"Game over. Your points {points}");
    }
    
    public void MigrationPlatformAndScale ()
    {
        StickHeroPlatform migrationPlatform = m_Platforms[counter];
        StickHeroPlatform lastPlatform = m_Platforms[counter == 0 ? 4 : counter - 1];

        System.Random random = new System.Random();
        float randomScaleMigrationPlatform = random.Next((int)(m_Player.transform.localScale.x * 100), 100) * 0.01f;
        migrationPlatform.transform.localScale = new Vector3(randomScaleMigrationPlatform, m_Platforms[0].transform.localScale.y, m_Platforms[0].transform.localScale.z);
        
        float randomLengthToMigrationPlatform = lastPlatform.transform.position.x + lastPlatform.GetPlatformSize() * 0.5f + random.Next((int)(m_Player.transform.localScale.x * 150), 120) * 0.01f + migrationPlatform.GetPlatformSize() * 0.5f;
        migrationPlatform.transform.position = new Vector3(randomLengthToMigrationPlatform, m_Platforms[0].transform.position.y, m_Platforms[0].transform.position.z);
    }
    public float GetLengthToEdgethirdPlatform()
    {
        int counterNextPlatform = counter == 4 ? 0 : counter + 1;
        StickHeroPlatform thirdPlatform = m_Platforms[counterNextPlatform == 4 ? 0 : counterNextPlatform + 1];
        float maxLengthStick = thirdPlatform.transform.position.x - m_Stick.transform.position.x- thirdPlatform.GetPlatformSize()*0.5f- m_Player.transform.localScale.x*1.5f;
        return maxLengthStick;
    }
}
