﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public enum Direction
{
    None = -1,
    Left = 0,
    Right = 1,
    Up = 2,
    Down = 3,
}

public class Neighbour
{
    public Direction Direction;
    public MatchThree_Cell Cell;
}

public class MatchThree_Cell : MonoBehaviour
{
    public MatchThree_Candy Candy;
    private readonly Neighbour[] neighbours = new Neighbour[4];
    public int CellId { get; private set; }
    public bool Move { get; private set; }

    public MatchThree_Cell GetNeighbour(Direction direction)
    {
        return neighbours.FirstOrDefault(n => n!=null && n.Direction == direction)?.Cell;
    }

    public void SetNeighbours(Direction direction, MatchThree_Cell cell)
    {
        if (GetNeighbour((direction)))
        {
            return;
        }

        neighbours[(int) direction] = new Neighbour()
        {
            Direction = direction,
            Cell = cell
        };
    }

    public void SetId(int id)
    {
        CellId = id;
    }
    // Start is called before the first frame update
    private void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        var cellDown = this.GetNeighbour(Direction.Down);
        if (cellDown==null)
        {
            return;
        }
        if (this.Candy == null&& this.GetNeighbour(Direction.Up)==null)
        {
            MatchThree_Controller.CreateCandy(this);
            return;
        }
        if (cellDown.Candy != null)
        {
            if (this.Candy != null)
            {
                Move = false;
            }
            return;
        }
        if (this.Candy == null)
        {
            return;
        }
        if (cellDown.transform.position.y < this.Candy.transform.position.y)
        {
            Move = true;
            this.Candy.transform.Translate(transform.up * Time.deltaTime * -1f, Space.World);
            return;
        }

        var CandyDown = Candy;
        Candy = null;
        cellDown.Candy = CandyDown;
        cellDown.Candy.transform.position = cellDown.transform.position;


    }
}
