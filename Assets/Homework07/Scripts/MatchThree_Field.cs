﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MatchThree_Field : MonoBehaviour
{
    [SerializeField] private Camera m_MainCamera;
    [SerializeField] private MatchThree_Controller m_Controller;
    [SerializeField] private GameObject m_Cell;
    [SerializeField] private float m_CellSize = 0.6f;
    [SerializeField] private int m_FieldWidth = 6;
    [SerializeField] private int m_FieldHeight = 8; 

    private static readonly List<List<MatchThree_Cell>> GameField = new List<List<MatchThree_Cell>>();

    public static float CurrentCellSize;

    public void Init()
    {
        GenerateField(m_FieldWidth,m_FieldHeight);
        CurrentCellSize = m_CellSize;
    }

    private void GenerateField(int width, int height)
    {
        int CellId = 0;
        for (int x = 0; x < width; x++)
        {
            GameField.Add(new List<MatchThree_Cell>());
            for (int y = 0; y < height; y++)
            {
                Vector3 pos = new Vector3(x*m_CellSize,y*m_CellSize,0f);
                var obj = Instantiate(m_Cell, pos, Quaternion.identity);
                obj.name = $"Cell {x} {y}";

                var cell = obj.AddComponent<MatchThree_Cell>();
                cell.SetId(CellId);
                CellId++;
                GameField[x].Add(cell);
                
                //настройка соседей по горизонтали
                
                //если не крайняя левая клетка
                if (x>0)
                {
                    cell.SetNeighbours(Direction.Left,GameField[x-1][y]);
                    GameField[x-1][y].SetNeighbours(Direction.Right,cell);
                }

                if (y>0)
                {
                    cell.SetNeighbours(Direction.Down,GameField[x][y-1]);
                    GameField[x][y-1].SetNeighbours(Direction.Up,cell);
                }
            }
        }
        var cameraPosition = new  Vector3(width*m_CellSize*0.5f-0.5f* m_CellSize, height*m_CellSize*0.5f-0.5f* m_CellSize, -50f);
        m_MainCamera.transform.position = cameraPosition;
    }
    public static MatchThree_Cell GetCell(MatchThree_Candy candy)
    {
        foreach (var row in GameField)
        {
            foreach (var cell in row)
            {
                if (cell.Candy == candy)
                {
                    return cell;
                }
            }
        }

        return null;
    }

    public static MatchThree_Cell GetCell(int x, int y)
    {
        return GameField[x][y];
    }
    public bool CheckCandyNull()
    {
        for (int x = m_FieldWidth - 1; x >= 0; x--)
        {

            for (int y = 0; y < m_FieldHeight; y++)
            {
                if (GameField[x][y].Candy == null)
                {
                    return true;
                }
            }
        }
        return false;
    }
    public bool CheckField()
    {
        List<MatchThree_Cell> cellsCandyForDestroy = new List<MatchThree_Cell>();
        for (int x = m_FieldWidth - 1; x >= 0; x--)
        {

            for (int y = 0; y < m_FieldHeight; y++)
            {
                if (GameField[x][y].Candy==null)
                {
                    return true;
                }
                bool isFreeTargetPlacement = MatchThree_Controller.IsFreeCandyPlacement(GameField[x][y], GameField[x][y].Candy.CandyData.Id);
                if (!isFreeTargetPlacement)
                {
                    cellsCandyForDestroy.AddRange(MatchThree_Controller.GetCellsForDestroy(MatchThree_Controller.GetCellsWithTheSameColor(GameField[x][y], GameField[x][y].Candy.CandyData.Id)));
                }
                //если смена НЕ приведет к совпадению 3-х
                if (isFreeTargetPlacement)
                {
                    continue;
                }

                //Если приведет к совпадению 3-х
                if (!isFreeTargetPlacement)
                {
                    cellsCandyForDestroy.Add(GameField[x][y]);
                }

                MatchThree_Controller.DestroyCandy(cellsCandyForDestroy);
            }
        }
        if (cellsCandyForDestroy.Count == 0)
        {
            MatchThree_Controller.ResetFactorX();
        }
        return false;
    }


    public bool CheckMove()
    {
        for (int x = m_FieldWidth - 1; x >= 0; x--)
        {
            for (int y = 0; y < m_FieldHeight-1; y++)
            {
                if (GameField[x][y].Move)
                {
                    return true;
                }
            }
        }
        return false;
    }
    public List<List<MatchThree_Cell>> CheckHunt()
    {
        List<List<MatchThree_Cell>> listHints = new List<List<MatchThree_Cell>>();
        for (int x = m_FieldWidth - 1; x >= 0; x--)
        {
            for (int y = 0; y < m_FieldHeight; y++)
            {
                if (GameField[x][y].Candy == null)
                {
                    return listHints;
                }
                for (int i = 0; i < 4; i++)
                {
                    listHints.Add(CheckHuntDirection(x, y, (Direction)i));
                }
            }
        }
        DeleteNullHunt(listHints);
        return listHints;
    }
    private List<List<MatchThree_Cell>> DeleteNullHunt(List<List<MatchThree_Cell>>  Hints)
    {
        for (int i = 0; i < Hints.Count; i++)
        {
            if (Hints[i].Count<1)
            {
                Hints.RemoveAt(i);
                i--;
            }
        }
        return Hints;
    }
    public List<MatchThree_Cell> CheckHuntDirection(int x, int y,Direction direction)
    {
        List<MatchThree_Cell> hint = new List<MatchThree_Cell>();
        var Cell = GameField[x][y].GetNeighbour(direction);
        if (Cell == null)
        {
            return hint;
        }
        if (GameField[x][y].Candy.CandyData.Id== Cell.Candy.CandyData.Id)
        {
            var nextCell = Cell.GetNeighbour(direction);
            if (nextCell==null)
            {
                return hint;
            }
            var targetCell = FindCandy(nextCell, GameField[x][y].Candy.CandyData.Id, direction);
            if (targetCell == null)
            {
                return hint;
            }
            hint.Add(targetCell);
            hint.Add(nextCell);
            return hint;
        }
        return hint;
    }
    private MatchThree_Cell FindCandy(MatchThree_Cell cell, int id, Direction direction)
    {
        direction = inversionDirection(direction);
        for (int i = 0; i < 4; i++)
        {
            if (direction== (Direction)i)
            {
                continue;
            }
            var targetCell = cell.GetNeighbour((Direction)i);
            if (targetCell==null)
            {
                continue;
            }
            if(targetCell.Candy.CandyData.Id == id)
            {
                return targetCell;
            }
        }
        return null;
    }
    private Direction inversionDirection(Direction direction)
    {
        switch (direction)
        {
            case Direction.Left:
                return Direction.Right;
            case Direction.Right:
                return Direction.Left;
            case Direction.Up:
                return Direction.Down;
            case Direction.Down:
                return Direction.Up;
            default:
                return Direction.None;
        }
    }
}
