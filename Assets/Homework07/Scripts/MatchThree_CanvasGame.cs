﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MatchThree_CanvasGame : MonoBehaviour
{
    [SerializeField] Text m_TextPoint;
    [SerializeField] Text m_TextFactorX;
    [SerializeField] Text m_TextName;
    [SerializeField] private Camera m_MainCamera;
    private float colorAlfa;
    private float speedAlfa = 0.2f;

    public void CanvasActive(bool active)
    {
        var canvas = this.GetComponent<Canvas>();
        if (active)
        {
            canvas.planeDistance = 1;
        }
        else
        {
            canvas.planeDistance = -1;
        }
    }
    public void MoveCameraGameOver()
    {
        var pos = m_MainCamera.transform.position;
        pos.z = -50;
        m_MainCamera.transform.position = pos;
    }
    void Start()
    {
        m_TextPoint.text = "Point  " + 0.ToString("d9");
        var colorText = m_TextFactorX.color;
        colorAlfa =0f;
        colorText.a = colorAlfa;
        m_TextFactorX.color = colorText;
        CanvasActive(false);
    }

    public void SetTextPoint(int point)
    {
        m_TextPoint.text = "Point  " + point.ToString("d9");
    }
    public void SetTextFactorX(int FactorX)
    {
        m_TextFactorX.text = $"X{FactorX}";
        colorAlfa = 1f;
    }
    public string GetName()
    {
        return m_TextName.text;
    }
    // Update is called once per frame
    void Update()
    {
        if (colorAlfa < 0.01f)
        {
            return;
        }
        colorAlfa -= speedAlfa * Time.deltaTime;
        var colorText = m_TextFactorX.color;
        colorText.a = colorAlfa;
        m_TextFactorX.color = colorText;
    }
}
