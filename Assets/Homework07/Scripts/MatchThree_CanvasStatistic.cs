﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MatchThree_CanvasStatistic : MonoBehaviour
{
    [SerializeField] MatchThree_StatisticText[] m_StatisticTexts;


    void Start()
    {
        CanvasActive(false);
        GetStatistic();

    }
    public void CanvasActive(bool active)
    {
        var canvas = this.GetComponent<Canvas>();
        if (active)
        {
            canvas.planeDistance = 1;
        }
        else
        {
            canvas.planeDistance = -1;
        }
    }
    public int GetTopPlayer()
    {
        return m_StatisticTexts[0].Point;
    }
    public void SetUserPoints(string name, int points)
    {
        for (int i = 0; i < m_StatisticTexts.Length; i++)
        {
            var oldPoints = m_StatisticTexts[i].Point;
            if (points< oldPoints)
            {
                continue;
            }
            var oldName = m_StatisticTexts[i].Name;
            m_StatisticTexts[i].SetNameAndPoint(name, points);
            PlayerPrefs.SetInt(i.ToString(), points);
            PlayerPrefs.Save();
            PlayerPrefs.SetString(i.ToString()+"name", name);
            PlayerPrefs.Save();
            print("save");
            name = oldName;
            points = oldPoints;
        }
    }
    public void GetStatistic()
    {
        for (int i = 0; i < m_StatisticTexts.Length; i++)
        {
            var points = PlayerPrefs.GetInt(i.ToString());
            if (points <1)
            {
                m_StatisticTexts[i].SetNameAndPoint("no name", 0);
            }
            else
            {
                var name = PlayerPrefs.GetString(i.ToString() + "name");
                m_StatisticTexts[i].SetNameAndPoint($"{name}", points);
            }
        }
    }
    public void DeleteStatistic()
    {
        PlayerPrefs.DeleteAll();
        GetStatistic();
    }
}
