﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MatchThree_Candy : MonoBehaviour
{
    public MatchThree_CandyData CandyData;
    private Vector3 baseCandyPosition;
    private bool inDrag;
    private Collider2D currentCollider;



    private void OnMouseDown()
    {
        baseCandyPosition = transform.position;
        inDrag = true;
    }


    private void OnMouseDrag()
    {
        if (!inDrag)
        {
            return;
        }

        var position = MatchThree_Controller.MainCamera.ScreenToWorldPoint(Input.mousePosition);
        position.z = baseCandyPosition.z;
        //максимальное смещение конфеты
        float maxDistance = MatchThree_Field.CurrentCellSize;

        float x = position.x;
        float baseX = baseCandyPosition.x;
        float y = position.y;
        float baseY = baseCandyPosition.y;

        //ограничение смещения до одной ячейки
        x = Mathf.Clamp(x, baseX - maxDistance, baseX + maxDistance);
        y = Mathf.Clamp(y, baseY - maxDistance, baseY + maxDistance);
        //ограничение смещения по одной оси
        if (Mathf.Abs(x - baseX) > Mathf.Abs(y - baseY))
        {
            y = baseY;
        }
        else
        {
            x = baseX;
        }


        position.x = x;
        position.y = y;

        transform.position = position;
    }

    private void OnMouseUp()
    {
        inDrag = false;
        if (currentCollider)
        {
            MatchThree_Candy targetCandy = currentCollider.GetComponent<MatchThree_Candy>();
            var targetCell = MatchThree_Field.GetCell(targetCandy);
            var currentCell = MatchThree_Field.GetCell(this);
            //пытаемся поменять ячейки
            currentCell.Candy = targetCandy;
            targetCell.Candy = this;

            //проверить валидность
            List<MatchThree_Cell> cellsCandyForDestroy = new List<MatchThree_Cell>();

            bool isFreeTargetPlacement = MatchThree_Controller.IsFreeCandyPlacement(targetCell, CandyData.Id);
            if (!isFreeTargetPlacement)
            {
                cellsCandyForDestroy.AddRange(MatchThree_Controller.GetCellsForDestroy(MatchThree_Controller.GetCellsWithTheSameColor(targetCell, CandyData.Id)));
            }

            bool isFreeCurrentPlacement = MatchThree_Controller.IsFreeCandyPlacement(currentCell, targetCandy.CandyData.Id);
            if (!isFreeCurrentPlacement)
            {
                cellsCandyForDestroy.AddRange(MatchThree_Controller.GetCellsForDestroy(MatchThree_Controller.GetCellsWithTheSameColor(currentCell, targetCandy.CandyData.Id)));
            }
            //если смена НЕ приведет к совпадению 3-х
            if (isFreeCurrentPlacement&& isFreeTargetPlacement)
            {
                targetCell.Candy = targetCandy;
                currentCell.Candy = this;
                transform.position = baseCandyPosition;
                return;
            }

            //Если приведет к совпадению 3-х
            transform.position = targetCell.transform.position;
            targetCandy.transform.position = currentCell.transform.position;
            if (!isFreeTargetPlacement)
            {
                cellsCandyForDestroy.Add(targetCell);
            }
            if (!isFreeCurrentPlacement)
            {
                cellsCandyForDestroy.Add(currentCell);
            }
            MatchThree_Controller.DestroyCandy(cellsCandyForDestroy);

            return;
        }
        
        transform.position = baseCandyPosition;
    }

    //private void MoveDown(MatchThree_Cell cell)
    //{
    //    var CellUp = cell.GetNeighbour(Direction.Up);
    //    MatchThree_Candy targetCandyUp = CellUp.Candy.GetComponent<MatchThree_Candy>();
    //    var CellDown = cell;
    //    targetCandyUp.transform.position = cell.transform.position;
    //}
    private void OnTriggerEnter2D(Collider2D other)
    {
        currentCollider = other;
        
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if (currentCollider == other)
        {
            currentCollider = null;
        }
    }
    private void Update()
    {
        
        //if (Move)
        //{
        //    if (firstPos)
        //    {
        //        firstPositionY = this.transform.position.y;
        //        firstPos = false;
        //    }
        //    this.transform.Translate(transform.up * Time.deltaTime * -1f, Space.World);
        //    if (firstPositionY - this.transform.position.y > MatchThree_Field.CurrentCellSize)
        //    {
        //        Move = false;
        //        firstPos = true;
        //    }
        //}
    }
}