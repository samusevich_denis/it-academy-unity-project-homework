﻿using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;

public class MatchThree_Controller : MonoBehaviour
{
    [SerializeField] private MatchThree_Field m_Field;
    [SerializeField] private MatchThree_Types m_Types;
    [SerializeField] private MatchThree_CanvasGame m_CanvasGame;
    [SerializeField] private MatchThree_CanvasStatistic m_CanvasStatistic;

    private static int points;
    private static int lastPoints;
    public static Camera MainCamera;
    private static MatchThree_Types types;
    private static bool check;
    private static int factorX;
    private static int lastFactorX;
    /// <summary>
    ///проверяем можем ли мы поместить в данную ячейку такую кофету -  нужно избегать ситуаций с 3 и более в ряд на старте игры
    /// </summary>
    /// <param name="targetCell">Стартовая клетка</param>
    /// <param name="id">Id конфеты</param>
    /// <returns></returns>
    public static bool IsFreeCandyPlacement(MatchThree_Cell targetCell, int id)
    {
        var candysFourSides = GetCellsWithTheSameColor(targetCell, id);
        for (int i = 0; i < 2; i++)
        {
            if (candysFourSides[i*2].Count + candysFourSides[i*2+1].Count>1)
            {
                return false;
            }
        }
        return true;
    }
    public static List<MatchThree_Cell> GetCellsForDestroy(List<List<MatchThree_Cell>> cellsWithTheSameColor)
    {
        var returnCells = new List<MatchThree_Cell>();
        for (int i = 0; i < 2; i++)
        {
            if (cellsWithTheSameColor[i * 2].Count + cellsWithTheSameColor[i * 2 + 1].Count > 1)
            {
                for (int j = 0; j < cellsWithTheSameColor[i * 2].Count; j++)
                {
                    returnCells.Add(cellsWithTheSameColor[i * 2][j]);
                }
                for (int j = 0; j < cellsWithTheSameColor[i * 2+1].Count; j++)
                {
                    returnCells.Add(cellsWithTheSameColor[i * 2+1][j]);
                }
            }
        }
        return returnCells;
    }


    public static List<List<MatchThree_Cell>> GetCellsWithTheSameColor(MatchThree_Cell targetCell, int id)
    {
        var candysFourSides = new List<List<MatchThree_Cell>>();
        for (int i = 0; i < 4; i++)
        {
            candysFourSides.Add(new List<MatchThree_Cell>());
            Direction direction = (Direction)i;
            MatchThree_Cell cell = targetCell;

            while (true)
            {
                cell = cell.GetNeighbour(direction);
                if (!cell || !cell.Candy)
                {
                    break;
                }
                if (cell.Candy.CandyData.Id == id)
                {
                    candysFourSides[i].Add(cell);
                }
                else
                {
                    break;
                }
            }
        }
        return candysFourSides;
    }
    private void Start()
    {
        MainCamera = Camera.main;
        types = m_Types;
        m_Field.Init();

        var firstCell = MatchThree_Field.GetCell(0, 0);

        MatchThree_Cell cell = firstCell;
        while (cell)
        {
            SetupCandiesLine(cell, Direction.Right);
            cell = cell.GetNeighbour(Direction.Up);
        }
        check = false;
        factorX = 0;
        lastFactorX =0;
    }

    private void Update()
    {
        if (points>lastPoints)
        {
            m_CanvasGame.SetTextPoint(points);
            lastPoints = points;
        }
        if (factorX>lastFactorX)
        {
            m_CanvasGame.SetTextFactorX(factorX);
            lastFactorX = factorX;
        }
        if (m_Field.CheckMove())
        {
            check = true;
            return;
        }
        if (m_Field.CheckCandyNull())
        {
            return;
        }
        if (check)
        {
            check = m_Field.CheckField();
            return;
        }

    }
    public static void ResetFactorX()
    {
        factorX = 0;
        lastFactorX =0;
    }
    private void SetupCandiesLine(MatchThree_Cell firstCell, Direction direction)
    {
        MatchThree_Cell cell = firstCell;
        while (cell)
        {
            MatchThree_Candy newCandy = m_Types.GetRandomCandy();
            //пробуем генерить пока не получим разрешенную кoнефетку
            //типов конфеток должно быть 5 или более - иначе возможен вариант вечного цикла
            while (!IsFreeCandyPlacement(cell, newCandy.CandyData.Id))
            {
                Destroy(newCandy.gameObject);
                newCandy = m_Types.GetRandomCandy();
            }

            cell.Candy = newCandy;
            cell.Candy.transform.position = cell.transform.position;
            cell = cell.GetNeighbour(direction);
        }
    }

    public static void DestroyCandy(List<MatchThree_Cell> cellsCandyForDestroy)
    {
        factorX++;
        for (int i = 0; i < cellsCandyForDestroy.Count; i++)
        {
            if (cellsCandyForDestroy[i].Candy != null)
            {
                Destroy(cellsCandyForDestroy[i].Candy.gameObject);
                cellsCandyForDestroy[i].Candy = null;
                points += 5 * factorX;
            }
        }
        
    }

    public static void CreateCandy(MatchThree_Cell cell)
    {
        if (cell.Candy !=null)
        {
            return;
        }
        var newCandy = types.GetRandomCandy();
        while (!IsFreeCandyPlacement(cell, newCandy.CandyData.Id))
        {
            Destroy(newCandy.gameObject);
            newCandy = types.GetRandomCandy();
        }
        cell.Candy = newCandy;
        var position = cell.transform.position;
        cell.Candy.transform.position = position;
    }
    //public static void DeleteCellDuplicate(List<MatchThree_Cell> cells)
    //{
    //    for (int i = 0; i < cells.Count; i++)
    //    {
    //        for (int j = 0; j < cells.Count; j++)
    //        {
    //            if (i>=j)
    //            {
    //                continue;
    //            }
    //            if (cells[i].CellId == cells[j].CellId)
    //            {
    //                cells.RemoveAt(j);
    //                j--;
    //            }
    //        }
    //    }
    //}

    public void SetUserPoints()
    {

        var name = m_CanvasGame.GetName();
        m_CanvasStatistic.SetUserPoints(name, points);
        points = 0;
        lastPoints = 0;
        m_CanvasGame.SetTextPoint(0);
    }
    public int GetUserPoints()
    {
        return points;
    }
}