﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MatchThree_Hint : MonoBehaviour
{
    [SerializeField] private MatchThree_Field m_Field;
    [SerializeField] private MatchThree_CanvasStatistic m_CanvasStatistic;
    [SerializeField] private MatchThree_Controller m_Controller;
    [SerializeField] private GameObject m_Hints;
    private GameObject hint;
    private bool iteration;



    private float countSecond = 0;
    private List<List<MatchThree_Cell>> cellHints;

    // Start is called before the first frame update
    void Start()
    {
        cellHints = new List<List<MatchThree_Cell>>();
        hint = Instantiate(m_Hints);
        hint.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (m_Field.CheckMove())
        {
            if (hint.activeSelf)
            {
                hint.SetActive(false);
            }
            DeleteHint(cellHints);
            return;
        }
        if (countSecond<6)
        {
            countSecond += Time.deltaTime;
            return;
        }
        if (iteration)
        {
            hint.SetActive(false);
            iteration = false;
            countSecond = 0;
            return;
        }
        countSecond = 3;
        iteration = true;
        cellHints =m_Field.CheckHunt();
        if (m_CanvasStatistic.GetTopPlayer() > m_Controller.GetUserPoints())
        {
            GetHint();
        }
    }
    public void DeleteHint(List<List<MatchThree_Cell>> hints)
    {
        if (hints.Count==0)
        {
            return;
        }
        hints = new List<List<MatchThree_Cell>>();
    }
    public void GetHint()
    {
        var randomHint = cellHints[Random.Range(0, cellHints.Count)];
        ShowHint(randomHint);
    }
    public void ShowHint(List<MatchThree_Cell> randomHint)
    {
        float positionHintX;
        float positionHintY;
        hint.SetActive(true);
        var positionOne = randomHint[0].transform.position;
        var positionTwo = randomHint[1].transform.position;
        if (Mathf.Abs(positionOne.x - positionTwo.x)<0.1)
        {
            positionHintY = (positionOne.y + positionTwo.y) * 0.5f;
            positionHintX = positionOne.x;
            hint.transform.position = new Vector3(positionHintX, positionHintY, 0f);
            var rotation = hint.transform.rotation;
            rotation.z = 0;
            hint.transform.rotation = rotation;
        }

        if (Mathf.Abs(positionOne.y - positionTwo.y )< 0.1)
        {
            positionHintX = (positionOne.x + positionTwo.x) * 0.5f;
            positionHintY = positionOne.y;
            hint.transform.position = new Vector3(positionHintX, positionHintY, 0f);
            var rotation = hint.transform.rotation;
            rotation.eulerAngles = new Vector3(0f, 0f, 90f);
            hint.transform.rotation = rotation;
        }

    }
}
