﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MatchThree_StatisticText : MonoBehaviour
{
    public int index;
    public string Name { get; private set; }
    public int Point { get; private set; }
    private Text textStatistic;
    public void SetNameAndPoint(string name, int point)
    {
        Point = point;
        Name = name;
        textStatistic.text = $"{index}. {name}\n{point}";
    }


    // Start is called before the first frame update
    private void Awake()
    {
        textStatistic = this.GetComponent<Text>();
    }


    // Update is called once per frame
    void Update()
    {
        
    }
}
