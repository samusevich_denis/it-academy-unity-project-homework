﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MatchThree_CanvasMenu : MonoBehaviour
{
    [SerializeField] private Camera m_MainCamera;

    public void MoveCameraPlayGame()
    {
        var pos = m_MainCamera.transform.position;
        pos.z = -1;
        m_MainCamera.transform.position = pos;
    }
    public void CanvasActive(bool active)
    {
        var canvas = this.GetComponent<Canvas>();
        if (active)
        {
            canvas.planeDistance = 1;
        }
        else
        {
            canvas.planeDistance = -1;
        }
    }
    public void ExitGame()
    {
        Application.Quit();
    }
    // Start is called before the first frame update
    void Start()
    {
        CanvasActive(true);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
