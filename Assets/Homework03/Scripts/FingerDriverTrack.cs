﻿using System.Collections.Generic;
using UnityEngine;

public class FingerDriverTrack : MonoBehaviour
{
    private class Checkpoint
    {
        private Vector3[] Points = new Vector3[4];
        public bool IsPointInCheckpoint(Vector3 point)
        {
            return MathfTriangles.IsPointInTriangleXY(point, Points[0],Points[1],Points[2])||MathfTriangles.IsPointInTriangleXY(point, Points[2],Points[1],Points[3]);
        }
        public void CreateCheckpoint(TrackSegment firstSegment, TrackSegment secondSegment)
        {
            Points[0] = firstSegment.Points[0];
            Points[1] = firstSegment.Points[1];
            Points[2] = firstSegment.Points[2];
            Points[3] = secondSegment.Points[2];
        }
    }
    private class TrackSegment
    {
        public Vector3[] Points;
        public bool IsPointInSegment(Vector3 point)
        {
            return MathfTriangles.IsPointInTriangleXY(point, Points[0],Points[1],Points[2]);
        }
    }
    private class CacheSegment
    {
        public int[] PointCurrentSegment = new int[3];
        public List<int> IndexNeighboringSegments = new List<int>();
    }
    [SerializeField] LineRenderer m_lineRenderer;
    [SerializeField] private bool m_Debug;

    private Vector3[] corners;
    private TrackSegment[] segments;
    private int counterCheckpoint;
    private Checkpoint checkpointTrack = new Checkpoint();
    private int scoring;
    private CacheSegment[] cacheSegmentTrack;
    private int indexCurrentSegments;
    // Start is called before the first frame update
    void Start()
    {
        //Заполняем массив опорных точек
        corners = new Vector3[transform.childCount];
        for (int i = 0; i < corners.Length; i++)
        {
            GameObject obj = transform.GetChild(i).gameObject;
            corners[i] = obj.transform.position;
            obj.GetComponent<MeshRenderer>().enabled = false;           
        }
        
        // Настраиваем Line Renderer
        m_lineRenderer.positionCount = corners.Length;
        m_lineRenderer.SetPositions(corners);

        //Запекаем меж сетку из Line Renderer
        Mesh mesh = new Mesh();
        m_lineRenderer.BakeMesh(mesh, true);

        //создаем массив сегментов трассы
        // (каждый треугольник описан 3-мя точками из массива вершин)
        segments = new TrackSegment[mesh.triangles.Length/3];
        cacheSegmentTrack = new CacheSegment[mesh.triangles.Length/3];
        int segmentCounter = 0;
        for (int i = 0; i < mesh.triangles.Length; i+=3)
        {
            segments[segmentCounter] = new TrackSegment();
            segments[segmentCounter].Points = new Vector3[3];  
            segments[segmentCounter].Points[0]= mesh.vertices[mesh.triangles[i]];
            segments[segmentCounter].Points[1]= mesh.vertices[mesh.triangles[i+1]];
            segments[segmentCounter].Points[2]= mesh.vertices[mesh.triangles[i+2]];
            cacheSegmentTrack[segmentCounter]= new CacheSegment();
            cacheSegmentTrack[segmentCounter].PointCurrentSegment[0]=mesh.triangles[i];
            cacheSegmentTrack[segmentCounter].PointCurrentSegment[1]=mesh.triangles[i+1];
            cacheSegmentTrack[segmentCounter].PointCurrentSegment[2]=mesh.triangles[i+2];
            segmentCounter++;
        }

        for (int i = 0; i < cacheSegmentTrack.Length; i++)
        {
            for (int j = 0; j < cacheSegmentTrack.Length; j++)
            {
                for (int k = 0; k < cacheSegmentTrack[i].PointCurrentSegment.Length; k++)
                {
                    bool addSegment = false;
                    for (int l = 0; l < cacheSegmentTrack[i].PointCurrentSegment.Length; l++)
                    {
                        if (cacheSegmentTrack[i].PointCurrentSegment[k]==cacheSegmentTrack[j].PointCurrentSegment[l])
                        {
                            cacheSegmentTrack[i].IndexNeighboringSegments.Add(j);
                            addSegment = true;
                            break;
                        }
                    }
                    if (addSegment)
                    {
                        break;
                    }
                } 
            }
        }

        //отдельно можно продебажить сегменты
        if(!m_Debug)
        {
            return;
        }

        foreach (var segment in segments)
        {
            foreach (var point in segment.Points)
            {
                GameObject sphere = GameObject.CreatePrimitive(PrimitiveType.Sphere);
                sphere.transform.position = point;
                sphere.transform.localScale = Vector3.one*0.1f;
            }
        }

        scoring=0;
        counterCheckpoint = 2;
        CreateNewCheckpoint();
        for (int i = 0; i < segments.Length; i++)
        {
            if(segments[i].IsPointInSegment(new Vector3(0f,0f,0f)))
            {
                indexCurrentSegments= i;
                break;
            }
        }
    }
    
    public bool IsPointInTrack(Vector3 point)
    {
        IsPointInCurrentSegment(point);
        if(IsPointReachedCheckpoint(point))
        {
            scoring+=10;
            print($"Checkpoint!!!!   Score:{scoring}");
            CreateNewCheckpoint();
        }
        for (int i = 0; i < cacheSegmentTrack[indexCurrentSegments].IndexNeighboringSegments.Count; i++)
        {
            if(segments[cacheSegmentTrack[indexCurrentSegments].IndexNeighboringSegments[i]].IsPointInSegment(point))
            {
                return true;
            }
        }
        return false;
    }

    public void IsPointInCurrentSegment(Vector3 point)
    {
        if(!segments[indexCurrentSegments].IsPointInSegment(point))
        {
            for (int i = 0; i < cacheSegmentTrack[indexCurrentSegments].IndexNeighboringSegments.Count; i++)
            {
                if(segments[cacheSegmentTrack[indexCurrentSegments].IndexNeighboringSegments[i]].IsPointInSegment(point))
                {
                    indexCurrentSegments= cacheSegmentTrack[indexCurrentSegments].IndexNeighboringSegments[i];
                    break;
                }
            }
        }
    }
    
    public bool IsPointReachedCheckpoint(Vector3 point)
    {
        return checkpointTrack.IsPointInCheckpoint(point);
    }

    public void CreateNewCheckpoint()
    {
        checkpointTrack.CreateCheckpoint(segments[counterCheckpoint],segments[counterCheckpoint+1]);
        counterCheckpoint+=10;
    }
}
