﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct FitInTheHole_PosXY
{
    public int X {get;set;}
    public int Y {get;set;}
    public FitInTheHole_PosXY(int x, int y)
    {
        X=x;
        Y=y;
    }
}
