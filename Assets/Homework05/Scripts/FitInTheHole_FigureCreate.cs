﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FitInTheHole_FigureGame 
{
    public int HeightWall {get;}
    public int WidthWall {get;}
    public bool [,] FigurePrefab {get;  private set; }
    private bool [,] figureCheck;
    public FitInTheHole_FigureGame( int heightWall, int widthWall)
    {
        HeightWall = heightWall;
        WidthWall = widthWall;
    }
    public bool [,] CreateNewFigure(out List<FitInTheHole_PosXY> positionPlayer)
    {
        FigurePrefab = new bool[HeightWall, WidthWall];
        figureCheck = new bool[HeightWall, WidthWall];
        int centrFigure = (int)(WidthWall*0.5);
        FigurePrefab[0,centrFigure] = true;
        int countCube = 1;
        while(countCube< HeightWall*WidthWall*0.2f)
        {
            for (int y = 0; y < FigurePrefab.GetLength(0)-1; y++)
            {
                for (int x = 1; x < FigurePrefab.GetLength(1)-1; x++)
                {
                    if(FigurePrefab[y,x])
                    {
                        continue;
                    }
                    if (IsNeighboringCube(y,x))
                    {
                        if (Random.Range(0,2)>0)
                        {
                            FigurePrefab[y,x]=true;
                            countCube++;
                            break;
                        }
                    }
                }
            }
        }
        CheckFigure();
        for (int y = 0; y < FigurePrefab.GetLength(0); y++)
        {
            for (int x = 0; x < FigurePrefab.GetLength(1); x++)
            {
                if (figureCheck[y, x]|| FigurePrefab[y,x])
                {
                    continue;
                }
                else
                {
                    FigurePrefab[y, x] = true;
                }
            }
        }
        positionPlayer = GetPositionPlayer();
        return FigurePrefab;
    }
    private List<FitInTheHole_PosXY> GetPositionPlayer()
    {
        List<FitInTheHole_PosXY> positionPlayer = new List<FitInTheHole_PosXY>();
        for (int y = 0; y < FigurePrefab.GetLength(0); y++)
        {
            for (int x = 0; x < FigurePrefab.GetLength(1); x++)
            {
                if (FigurePrefab[y,x])
                {
                    continue;
                }
                if (IsNeighboringCube(y,x))
                {
                    positionPlayer.Add(new FitInTheHole_PosXY(x,y));
                }
            }
        }
        return positionPlayer;
    }
    private bool IsNeighboringCube (int y, int x)
    {   
        if (!(y==0))
        {
            if(FigurePrefab[y-1,x])
            {
                return true;
            }
        }
        if (!(y==HeightWall-1))
        {
            if(FigurePrefab[y+1,x])
            {
                return true;
            }
        }
        if (!(x==0))
        {
            if(FigurePrefab[y,x-1])
            {
                return true;
            }
        }
        if (!(x==WidthWall-1))
        {
            if(FigurePrefab[y,x+1])
            {
                return true;
            }
        }
        return false;
    }
    private void CheckFigure()
    {
        for (int y = 0; y < FigurePrefab.GetLength(0); y++)
        {
            for (int x = 0; x < FigurePrefab.GetLength(1); x++)
            {
                if (y== FigurePrefab.GetLength(0)-1|| x == FigurePrefab.GetLength(1)-1||x==0)
                {
                    figureCheck[y, x] = true;
                }
            }
        }
        for (int y = 0; y < FigurePrefab.GetLength(0)-1; y++)
        {
            for (int x = 1; x < FigurePrefab.GetLength(1)-1; x++)
            {
                if (!FigurePrefab[y,x]&& figureCheck[y,x-1])
                {
                    figureCheck[y, x] = true;
                }
                else
                {
                    break;
                }
            }
        }
        for (int y = 0; y < FigurePrefab.GetLength(0); y++)
        {
            for (int x = FigurePrefab.GetLength(1) - 2; x >0; x--)
            {
                if (!FigurePrefab[y, x] && figureCheck[y, x + 1])
                {
                    figureCheck[y, x] = true;
                }
                else
                {
                    break;
                }
            }
        }
        for (int x = 1; x < FigurePrefab.GetLength(1)-1; x++)
        {
            for (int y = FigurePrefab.GetLength(0)-2; y >-1; y--)
            {
                if (!FigurePrefab[y, x] && figureCheck[y+1, x])
                {
                    figureCheck[y, x] = true;
                }
                else
                {
                    break;
                }
            }
        }
        int iterator = 1;
        while (iterator<0)
        {
            for (int y = 0; y < FigurePrefab.GetLength(0) - 1; y++)
            {
                for (int x = 1; x < FigurePrefab.GetLength(1) - 1; x++)
                {
                    if (figureCheck[y, x] || FigurePrefab[y, x])
                    {
                        continue;
                    }
                    if (CheckNeighboringCubes(y,x))
                    {
                        figureCheck[y, x] = true;
                        iterator++;
                    }
                }
            }
            iterator--;
        }

    }
    private bool CheckNeighboringCubes(int x, int y)
    {
        if (!(y == 0))
        {
            if (figureCheck[y - 1, x])
            {
                return true;
            }
        }
        if (figureCheck[y + 1, x])
        {
            return true;
        }
        if (figureCheck[y, x - 1])
        {
            return true;
        }
        if (FigurePrefab[y, x + 1])
        {
            return true;
        }
        return false;
    }
}
