﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FitInTheHole_PositionCamera : MonoBehaviour
{
    [SerializeField] private FitInTheHole_LeveleGenerator m_LeveleGenerator;

    // Start is called before the first frame update
    void Start()
    {
        this.transform.position = new Vector3(m_LeveleGenerator.m_WidthWall*0.5f, this.transform.position.y, this.transform.position.z);
    }
}
