﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum FitInTheHole_EDirection
{
    none = 0,
    up = 1,
    upRight = 2,
    right = 3,
    downRight = 4,
    down = 5,
    downLeft = 6,
    left = 7,
    upLeft = 8,
}
