﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FitInTheHole_FigureTweener : MonoBehaviour
{
    private Vector3 fromPosition;
    private Vector3 toPosition;
    private Vector3 rotationPoint;
    private float rotationAngle;
    private float rotationDirection;
    private float speedMultiplayer = 4f;
    float timer;

    public void Tween(FitInTheHole_PosXY positionPlayer, float positionZ, FitInTheHole_EDirection directionKey, FitInTheHole_EDirection direction)
    {
        fromPosition = new Vector3 (positionPlayer.X,positionPlayer.Y, positionZ);
        FitInTheHole_PosXY nextPositionPlayer = FitInTheHole_FigureAndPlayer.GetNewPositionPlayer(positionPlayer,direction);
        toPosition = new Vector3 (nextPositionPlayer.X,nextPositionPlayer.Y, positionZ);

        rotationPoint =GetPositionRotate(directionKey,direction);
        if (directionKey==FitInTheHole_EDirection.right)
        {
            rotationDirection = -1;
        }
        if (directionKey==FitInTheHole_EDirection.left)
        {
            rotationDirection = 1;
        }
    }
    private Vector3 PositionRotateUpLeft()
    {
        return new Vector3 (fromPosition.x-0.5f,fromPosition.y+0.5f, 0f);
    }
    private Vector3 PositionRotateDownLeft()
    {
        return new Vector3 (fromPosition.x-0.5f,fromPosition.y-0.5f, 0f);
    }
    private Vector3 PositionRotateDownRight()
    {
        return new Vector3 (fromPosition.x+0.5f,fromPosition.y-0.5f, 0f);
    }
    private Vector3 PositionRotateUpRight()
    {
        return new Vector3 (fromPosition.x+0.5f,fromPosition.y+0.5f, 0f);
    }
    private Vector3 GetPositionRotate(FitInTheHole_EDirection directionKey, FitInTheHole_EDirection direction) 
    {
        switch (direction)
        {
            case FitInTheHole_EDirection.up:
            {
                rotationAngle = 90f;
                if(directionKey == FitInTheHole_EDirection.right)
                {
                    return PositionRotateUpRight();
                }
                else
                {
                    return PositionRotateUpLeft();
                }
            }
            case FitInTheHole_EDirection.upRight:
            {
                rotationAngle = 180f;
                return PositionRotateUpRight();
            }
            case FitInTheHole_EDirection.right:
            {
                rotationAngle = 90f;
                if(directionKey == FitInTheHole_EDirection.right)
                {
                    return PositionRotateDownRight();
                }
                else
                {
                    return PositionRotateUpRight();
                }
            }
            case FitInTheHole_EDirection.downRight:
            {
                rotationAngle = 180f;
                return PositionRotateDownRight();
            }
            case FitInTheHole_EDirection.down:
            {
                rotationAngle = 90f;
                if(directionKey == FitInTheHole_EDirection.right)
                {
                    return PositionRotateDownLeft();
                }
                else
                {
                    return PositionRotateDownRight();
                }
            }
            case FitInTheHole_EDirection.downLeft:
            {
                rotationAngle = 180f;
                return PositionRotateDownLeft();
            }
            case FitInTheHole_EDirection.left:
            {
                rotationAngle = 90f;
                if(directionKey == FitInTheHole_EDirection.right)
                {
                    return PositionRotateUpLeft();
                }
                else
                {
                    return PositionRotateDownLeft();
                }
            }
            case FitInTheHole_EDirection.upLeft:
            {
                rotationAngle = 180f;
                return PositionRotateUpLeft();
            }
            default:
            {   
                return new Vector3 (fromPosition.x,fromPosition.y,0f);
            }
        }
    }
    void Update()
    {
        transform.RotateAround(rotationPoint, Vector3.forward,
            Time.deltaTime * speedMultiplayer * rotationAngle * rotationDirection);

        timer += Time.deltaTime * speedMultiplayer;
        timer = Mathf.Clamp01(timer);

        if (timer < 0.999f)
            return;
        transform.position = toPosition;
        transform.rotation = Quaternion.identity;

        Destroy(this);
    }
}