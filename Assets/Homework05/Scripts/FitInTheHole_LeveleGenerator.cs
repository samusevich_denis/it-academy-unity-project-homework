﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;

public class FitInTheHole_LeveleGenerator : MonoBehaviour
{
    [SerializeField] private GameObject m_CubePrefab;
    [SerializeField] private GameObject m_PlayerPrefab;
    [SerializeField] private GameObject m_BadPlayer;
    [SerializeField] private float m_BaseSpeed = 2f;
    [SerializeField] private float m_WallDistance = 35f;
    [SerializeField] private int m_HeightWall = 6;
    [SerializeField] public int m_WidthWall = 12;
    [SerializeField] private int amountPlayerFifures= 2;
    private bool [][,] figurePrefabs;
    private GameObject [] figureGameObject;
    private FitInTheHole_FigureAndPlayer[] figureAndPlayer;
    private List<FitInTheHole_PosXY> [] positionsPlayerOnFigures;
    [SerializeField] private Transform m_FigurePointZ;
    private GameObject currentGameObject;
    private FitInTheHole_FigureAndPlayer currentFigure;
    private float speed;
    private FitInTheHole_Wall wall;
    private bool checkPlayerInHole = false;
    private int countLevel = 0;
    private bool gamePlay = false;

    private void Start()
    {
        this.transform.position = new Vector3(m_WidthWall * 0.5f, -0.5f, 0f);
        this.transform.localScale = new Vector3(m_WidthWall * 0.2f, 0f, m_FigurePointZ.position.z * 0.5f);

        FitInTheHole_FigureGame figureGame = new FitInTheHole_FigureGame(m_HeightWall, m_WidthWall);
        figurePrefabs = new bool[amountPlayerFifures][,];
        positionsPlayerOnFigures = new List<FitInTheHole_PosXY> [amountPlayerFifures];
        for (int i = 0; i < figurePrefabs.Length; i++)
        {
            List<FitInTheHole_PosXY> positionPlayerOnFigure = new List<FitInTheHole_PosXY>();
            figurePrefabs[i]= figureGame.CreateNewFigure(out positionPlayerOnFigure );
            positionsPlayerOnFigures[i] = positionPlayerOnFigure;
        }
        figureGameObject = new GameObject[amountPlayerFifures];
        figureAndPlayer = new FitInTheHole_FigureAndPlayer[amountPlayerFifures];
        for (int i = 0; i < figureGameObject.Length; i++)
        {
            figureGameObject[i] = new GameObject();
            figureAndPlayer[i] = figureGameObject[i].AddComponent<FitInTheHole_FigureAndPlayer>();
            figureAndPlayer[i].InstantiateFigure(figurePrefabs[i], m_CubePrefab, m_PlayerPrefab,m_BadPlayer, positionsPlayerOnFigures[i]);
            figureGameObject[i].gameObject.SetActive(false);
            figureAndPlayer[i].SetActiveFigure(false);
            figureAndPlayer[i].SetPositionFigure(m_FigurePointZ.position.z);
        }
        wall = new FitInTheHole_Wall(m_WidthWall, m_HeightWall, m_CubePrefab);
        var rand = Random.Range(0, amountPlayerFifures);
        currentFigure = figureAndPlayer[rand];
        currentGameObject = figureGameObject[rand];
        SetupFigure();
        wall.SetupWall(currentFigure.FigurePrefab, m_WallDistance,currentFigure.PositionHole);
        speed = m_BaseSpeed;
    }

    // Update is called once per frame
    private void Update()
    {
        if (wall.Parent.transform.position.z > m_WallDistance *-1f+1)
        {        
            wall.Parent.transform.Translate(speed * Time.deltaTime * Vector3.back);
            return;
        }
        if(wall.Parent.transform.position.z > m_WallDistance * -1f)
        {
            if (!checkPlayerInHole)
            {
                gamePlay = currentFigure.PlayerInHole(speed);
                checkPlayerInHole = true;
                return;
            }
            if (gamePlay)
            {
                wall.Parent.transform.Translate(speed  * Time.deltaTime * Vector3.back); 
                return;
            }
            wall.Parent.transform.Translate(speed* 0.3f* Time.deltaTime * Vector3.back);
            return;
        }
        if (gamePlay)
        {
            SetupFigure();
            wall.SetupWall(currentFigure.FigurePrefab, m_WallDistance, currentFigure.PositionHole);
            countLevel++;
            speed += 0.5f;
            print($"Good!!! Level {countLevel + 1} start.");
            checkPlayerInHole = false;
            gamePlay = false;
            return;
        }
        print($"Good!!! You have reached level {countLevel} and earned {Mathf.RoundToInt(countLevel* speed)} points.");
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    private void SetupFigure()
    {
        currentFigure.SetActiveFigure(false);
        currentGameObject.gameObject.SetActive(false);

        var rand = Random.Range(0,amountPlayerFifures);
        currentFigure = figureAndPlayer[rand];
        currentGameObject = figureGameObject[rand];
        currentFigure.SetupRandomPlayer();
        currentFigure.SetActiveFigure(true);
        currentGameObject.gameObject.SetActive(true);
    }
}
