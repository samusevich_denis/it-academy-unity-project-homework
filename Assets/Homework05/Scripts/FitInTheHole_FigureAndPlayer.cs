﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FitInTheHole_FigureAndPlayer : MonoBehaviour
{

    private GameObject cubeFigure;
    private GameObject cubePlayer;
    private GameObject badPlayer;
    private GameObject Player;
    private FitInTheHole_PosXY positionPlayer;
    public FitInTheHole_PosXY PositionHole {get; private set;}
    private FitInTheHole_EDirection directionLeft;
    private FitInTheHole_EDirection directionRight;
    private Transform parentCubesFigure;
    public bool [,] FigurePrefab {get; private set;}
    private List<FitInTheHole_PosXY> positionPlayerOnFigure;
    private FitInTheHole_FigureTweener tweener; //экземпляр класса, отвечающий за повороты кубика
    private bool playerStop = false;
    private float speedPlayer;
    public void InstantiateFigure(bool[,] figurePrefab, GameObject cubePrefabFigure, GameObject cubePrefabPlayer, GameObject cubePrefabBadPlayer, List<FitInTheHole_PosXY> positionsPlayer)
    {
        FigurePrefab = figurePrefab;
        cubeFigure = cubePrefabFigure;
        cubePlayer = cubePrefabPlayer;
        badPlayer = cubePrefabBadPlayer;
        positionPlayerOnFigure = positionsPlayer;
        parentCubesFigure = new GameObject().transform;
        for (int y = 0; y < FigurePrefab.GetLength(0); y++)
        {
            for (int x = 0; x < FigurePrefab.GetLength(1); x++)
            {
                if(FigurePrefab[y,x])
                {
                    var obj = Object.Instantiate(cubeFigure,new Vector3(x,y,0f), Quaternion.identity);
                    obj.transform.parent = parentCubesFigure;
                }
            }
        }
        badPlayer = Object.Instantiate(badPlayer, new Vector3(positionPlayer.X, positionPlayer.Y, 0f), Quaternion.identity);
        Player = Object.Instantiate(cubePlayer,new Vector3(positionPlayer.X,positionPlayer.Y,0f), Quaternion.identity);
        badPlayer.transform.parent = parentCubesFigure;
        Player.transform.parent = parentCubesFigure;
        badPlayer.SetActive(false);
        SetupRandomPlayer();
    }
    public void SetupRandomPlayer()
    {
        playerStop = false;
        positionPlayer = positionPlayerOnFigure[Random.Range(0, positionPlayerOnFigure.Count)];
        PositionHole = positionPlayerOnFigure[Random.Range(0, positionPlayerOnFigure.Count)];
        directionLeft = FindFirstDirectionLeft();
        directionRight = FindFirstDirectionRight();
        Player.SetActive(true);
        badPlayer.transform.position = Player.transform.position;
        badPlayer.SetActive(false);
        Player.transform.position = new Vector3(positionPlayer.X, positionPlayer.Y, parentCubesFigure.position.z);
    }
    public void SetPositionFigure (float positionZ)
    {
        parentCubesFigure.position = new Vector3(0f, 0f, positionZ);
    }
    public void SetActiveFigure(bool active)
    {
        parentCubesFigure.gameObject.SetActive(active);
    }
    public bool PlayerInHole(float speedWall)
    {
        speedPlayer = speedWall;
        playerStop = true;
        return PositionHole.X == positionPlayer.X && PositionHole.Y == positionPlayer.Y;
    }
    private void Update()
    {
        if (playerStop)
        {
            if(PositionHole.X != positionPlayer.X || PositionHole.Y != positionPlayer.Y)
            {
                if (!badPlayer.activeSelf)
                {
                    badPlayer.SetActive(true);
                    badPlayer.transform.position = Player.transform.position;
                    Player.SetActive(false);
                }
                badPlayer.transform.Translate(speedPlayer*0.3f * Time.deltaTime * Vector3.back);
            }
            return;
        }
        if (tweener)
            return;

        if (Input.GetKeyDown(KeyCode.LeftArrow))
            MoveLeft();

        if (Input.GetKeyDown(KeyCode.RightArrow))
            MoveRight();
    }

    private void MoveLeft()
    {
        if (IsMovementPossible(directionLeft))
            return;

        tweener = Player.gameObject.AddComponent<FitInTheHole_FigureTweener>();
        tweener.Tween(positionPlayer, parentCubesFigure.position.z,FitInTheHole_EDirection.left, directionLeft);
        positionPlayer = GetNewPositionPlayer(positionPlayer,directionLeft);
        directionRight= InversionDirection(directionLeft);
        directionLeft = FindDirectionLeft(directionLeft);
    }

    private void MoveRight()
    {
        if (IsMovementPossible(directionRight))
            return;

        tweener = Player.gameObject.AddComponent<FitInTheHole_FigureTweener>();
        tweener.Tween(positionPlayer, parentCubesFigure.position.z, FitInTheHole_EDirection.right, directionRight);
        positionPlayer = GetNewPositionPlayer(positionPlayer,directionRight);
        directionLeft = InversionDirection(directionRight);
        directionRight = FindDirectionRight(directionRight);
    }

    private bool IsMovementPossible(FitInTheHole_EDirection direction)
    {
        switch (direction)
        {
            case FitInTheHole_EDirection.up:
            {
                return positionPlayer.Y == FigurePrefab.GetLength(0)-1;
            }
            case FitInTheHole_EDirection.upRight:
            {
                return positionPlayer.X == FigurePrefab.GetLength(1)-1||positionPlayer.Y == FigurePrefab.GetLength(0)-1;
            }
            case FitInTheHole_EDirection.right:
            {
                return positionPlayer.X == FigurePrefab.GetLength(1)-1;
            }
            case FitInTheHole_EDirection.downRight:
            {
                return positionPlayer.X == FigurePrefab.GetLength(1) - 1 ||positionPlayer.Y == 0;
            }
            case FitInTheHole_EDirection.down:
            {
                return positionPlayer.Y == 0;
            }
            case FitInTheHole_EDirection.downLeft:
            {
                return positionPlayer.X == 0 ||positionPlayer.Y == 0;
            }
            case FitInTheHole_EDirection.left:
            {
                return positionPlayer.X == 0;
            }
            case FitInTheHole_EDirection.upLeft:
            {
                return positionPlayer.X == 0||positionPlayer.Y == FigurePrefab.GetLength(0) - 1;
            }
            default:
            return true;
        }
    }
    
    public static FitInTheHole_PosXY GetNewPositionPlayer(FitInTheHole_PosXY position, FitInTheHole_EDirection direction)
    {
        switch (direction)
        {
            case FitInTheHole_EDirection.up:
            {
                return new FitInTheHole_PosXY(position.X,position.Y+1);
            }
            case FitInTheHole_EDirection.upRight:
            {
                return new FitInTheHole_PosXY(position.X+1,position.Y+1);
            }
            case FitInTheHole_EDirection.right:
            {
                return new FitInTheHole_PosXY(position.X+1,position.Y);
            }
            case FitInTheHole_EDirection.downRight:
            {
                return new FitInTheHole_PosXY(position.X+1,position.Y-1);
            }
            case FitInTheHole_EDirection.down:
            {
                return new FitInTheHole_PosXY(position.X,position.Y-1);
            }
            case FitInTheHole_EDirection.downLeft:
            {
                return new FitInTheHole_PosXY(position.X-1,position.Y-1);
            }
            case FitInTheHole_EDirection.left:
            {
                return new FitInTheHole_PosXY(position.X-1,position.Y);
            }
            case FitInTheHole_EDirection.upLeft:
            {
                return new FitInTheHole_PosXY(position.X-1,position.Y+1);
            }
            default:
            {   
                return position;
            }
        }
    }
    
    private FitInTheHole_EDirection InversionDirection(FitInTheHole_EDirection direction)
    {
        switch (direction)
        {
            case FitInTheHole_EDirection.up:
            {
                return FitInTheHole_EDirection.down;
            }
            case FitInTheHole_EDirection.upRight:
            {
                return FitInTheHole_EDirection.downLeft;
            }
            case FitInTheHole_EDirection.right:
            {
                return FitInTheHole_EDirection.left;
            }
            case FitInTheHole_EDirection.downRight:
            {
                return FitInTheHole_EDirection.upLeft;
            }
            case FitInTheHole_EDirection.down:
            {
                return FitInTheHole_EDirection.up;
            }
            case FitInTheHole_EDirection.downLeft:
            {
                return FitInTheHole_EDirection.upRight;
            }
            case FitInTheHole_EDirection.left:
            {
                return FitInTheHole_EDirection.right;
            }
            case FitInTheHole_EDirection.upLeft:
            {
                return FitInTheHole_EDirection.downRight;
            }
            default:
            {   
                return FitInTheHole_EDirection.none;
            }
        }
    }

    private bool CheckPositionUp()
    {
        if(positionPlayer.Y == FigurePrefab.GetLength(0)-1)
        {
            return false;
        }
        else
        {
            return FigurePrefab[positionPlayer.Y+1,positionPlayer.X];
        }
    }
    private bool CheckPositionUpLeft()
    {
        if(positionPlayer.X == 0 ||positionPlayer.Y == FigurePrefab.GetLength(0) - 1)
        {
            return false;
        }
        else
        {
            return FigurePrefab[positionPlayer.Y+1,positionPlayer.X-1];
        }
    }
    private bool CheckPositionLeft()
    {
        if(positionPlayer.X == 0)
        {
            return false;
        }
        else
        {
            return FigurePrefab[positionPlayer.Y, positionPlayer.X - 1];
        }
    }
    private bool CheckPositionDownLeft()
    {
        if(positionPlayer.X == 0||positionPlayer.Y == 0)
        {
            return false;
        }
        else
        {
            return FigurePrefab[positionPlayer.Y-1,positionPlayer.X-1];
        }
    }
    private bool CheckPositionDown()
    {
        if(positionPlayer.Y == 0)
        {
            return false;
        }
        else
        {
            return FigurePrefab[positionPlayer.Y-1,positionPlayer.X];
        }
    }
    private bool CheckPositionDownRight()
    {
        if(positionPlayer.Y == 0||positionPlayer.X == FigurePrefab.GetLength(1)-1)
        {
            return false;
        }
        else
        {
            return FigurePrefab[positionPlayer.Y-1,positionPlayer.X+1];
        }
    }
    private bool CheckPositionRight()
    {
        if(positionPlayer.X == FigurePrefab.GetLength(1)-1)
        {
            return false;
        }
        else
        {
            return FigurePrefab[positionPlayer.Y,positionPlayer.X+1];
        }
    }
    private bool CheckPositionUpRight()
    {
        if(positionPlayer.X == FigurePrefab.GetLength(1)-1||positionPlayer.Y == FigurePrefab.GetLength(0)-1)
        {
            return false;
        }
        else
        {
            return FigurePrefab[positionPlayer.Y+1,positionPlayer.X+1];
        }
    }
    
    private FitInTheHole_EDirection FindDirectionLeft(FitInTheHole_EDirection direction)
    {
        for (int i = 0; i < 2; i++)
        {
            if (direction == FitInTheHole_EDirection.up || direction == FitInTheHole_EDirection.upRight)
            {
                if (!CheckPositionUpLeft())
                {
                    return FitInTheHole_EDirection.upLeft;
                }
                else if (!CheckPositionUp())
                {
                    return FitInTheHole_EDirection.up;
                }
                direction = FitInTheHole_EDirection.right;
            }
            if (direction == FitInTheHole_EDirection.right || direction == FitInTheHole_EDirection.downRight)
            {
                if (!CheckPositionUpRight())
                {
                    return FitInTheHole_EDirection.upRight;
                }
                else if (!CheckPositionRight())
                {
                    return FitInTheHole_EDirection.right;
                }
                direction = FitInTheHole_EDirection.down;
            }
            if (direction == FitInTheHole_EDirection.down || direction == FitInTheHole_EDirection.downLeft)
            {
                if (!CheckPositionDownRight())
                {
                    return FitInTheHole_EDirection.downRight;
                }
                else if (!CheckPositionDown())
                {
                    return FitInTheHole_EDirection.down;
                }
                direction = FitInTheHole_EDirection.left;
            }
            if (direction == FitInTheHole_EDirection.left || direction == FitInTheHole_EDirection.upLeft)
            {
                if (!CheckPositionDownLeft())
                {
                    return FitInTheHole_EDirection.downLeft;
                }
                else if (!CheckPositionLeft())
                {
                    return FitInTheHole_EDirection.left;
                }
                direction = FitInTheHole_EDirection.up;
            }
        }
        return FitInTheHole_EDirection.none;
        //for (int i = 0; i < 2; i++)
        //{
            //switch (direction)
            //{
            //    case FitInTheHole_EDirection.up:
            //    {
            //        if(!CheckPositionUpLeft())
            //        {
            //            return FitInTheHole_EDirection.upLeft;
            //        }
            //        break;
            //    }
            //    case FitInTheHole_EDirection.upRight:
            //    {
            //        if(!CheckPositionUpLeft())
            //        {
            //            return FitInTheHole_EDirection.upLeft;
            //        }
            //        break;
            //    }
            //    case FitInTheHole_EDirection.right:
            //    {
            //        if(CheckPositionUp()&& !CheckPositionUpRight())
            //        {
            //            return FitInTheHole_EDirection.upLeft;
            //        }
            //        break;
            //    }
            //    case FitInTheHole_EDirection.downRight:
            //    {
            //        if(CheckPositionUpRight() && !CheckPositionRight())
            //        {
            //            return FitInTheHole_EDirection.left;
            //        }
            //        break;           
            //    }
            //    case FitInTheHole_EDirection.down:
            //    {
            //        if(CheckPositionRight() && !CheckPositionDownRight())
            //        {
            //            return FitInTheHole_EDirection.downLeft;
            //        }
            //        break;
            //    }
            //    case FitInTheHole_EDirection.downLeft:
            //    {
            //        if(CheckPositionDownRight() && !CheckPositionDown())
            //        {
            //            return FitInTheHole_EDirection.down;
            //        }
            //        break;
            //    }
            //    case FitInTheHole_EDirection.left:
            //    {
            //        if(CheckPositionDown() && !CheckPositionDownLeft())
            //        {
            //            return FitInTheHole_EDirection.downRight;
            //        }
            //        break;
            //    }
            //    case FitInTheHole_EDirection.upLeft:
            //    {
            //        if(CheckPositionDownLeft() && !CheckPositionLeft())
            //        {
            //            return FitInTheHole_EDirection.right;
            //        }
            //        break;
            //    }
            //    default:
            //    {   
            //        direction= direction==FitInTheHole_EDirection.upLeft?direction =FitInTheHole_EDirection.up:direction+1;
            //        break;
            //    }
            //}
        //}
        //return FitInTheHole_EDirection.none;
    }
    private FitInTheHole_EDirection FindDirectionRight(FitInTheHole_EDirection direction)
    {
        for (int i = 0; i < 2; i++)
        {
            if (direction == FitInTheHole_EDirection.up || direction == FitInTheHole_EDirection.upLeft)
            {
                if (!CheckPositionUpRight())
                {
                    return FitInTheHole_EDirection.upRight;
                }
                else if (!CheckPositionUp())
                {
                    return FitInTheHole_EDirection.up;
                }
                direction = FitInTheHole_EDirection.left;
            }
            if (direction == FitInTheHole_EDirection.left || direction == FitInTheHole_EDirection.downLeft)
            {
                if (!CheckPositionUpLeft())
                {
                    return FitInTheHole_EDirection.upLeft;
                }
                else if (!CheckPositionLeft())
                {
                    return FitInTheHole_EDirection.left;
                }
                direction = FitInTheHole_EDirection.down;
            }
            if (direction == FitInTheHole_EDirection.down || direction == FitInTheHole_EDirection.downRight)
            {
                if (!CheckPositionDownLeft())
                {
                    return FitInTheHole_EDirection.downLeft;
                }
                else if (!CheckPositionDown())
                {
                    return FitInTheHole_EDirection.down;
                }
                direction = FitInTheHole_EDirection.right;
            }
            if (direction == FitInTheHole_EDirection.right || direction == FitInTheHole_EDirection.upRight)
            {
                if (!CheckPositionDownRight())
                {
                    return FitInTheHole_EDirection.downRight;
                }
                else if (!CheckPositionRight())
                {
                    return FitInTheHole_EDirection.right;
                }
                direction = FitInTheHole_EDirection.up;
            }
        }
        return FitInTheHole_EDirection.none;
    }
    private FitInTheHole_EDirection FindFirstDirectionRight()
    {
        if (CheckPositionUp())
        {
            return FindDirectionRight(FitInTheHole_EDirection.left);
        }
        if (CheckPositionLeft())
        {
            return FindDirectionRight(FitInTheHole_EDirection.down);
        }
        if (CheckPositionDown())
        {
            return FindDirectionRight(FitInTheHole_EDirection.right);
        }
        if (CheckPositionRight())
        {
            return FindDirectionRight(FitInTheHole_EDirection.up);
        }
        return FitInTheHole_EDirection.none;
    }
    private FitInTheHole_EDirection FindFirstDirectionLeft()
    {
        if (CheckPositionUp())
        {
            return FindDirectionLeft(FitInTheHole_EDirection.right);
        }
        if (CheckPositionLeft())
        {
            return FindDirectionLeft(FitInTheHole_EDirection.up);
        }
        if (CheckPositionDown())
        {
            return FindDirectionLeft(FitInTheHole_EDirection.left);
        }
        if (CheckPositionRight())
        {
            return FindDirectionLeft(FitInTheHole_EDirection.down);
        }
        return FitInTheHole_EDirection.none;
        //if (CheckPositionUp()&& !CheckPositionUpRight())
        //{
        //    return FitInTheHole_EDirection.upLeft;
        //}
        //if(CheckPositionUpRight() && !CheckPositionRight())
        //{
        //    return FitInTheHole_EDirection.left;
        //}
        //if(CheckPositionRight() && !CheckPositionDownRight())
        //{
        //    return FitInTheHole_EDirection.downLeft;
        //}
        //if(CheckPositionDownRight() && !CheckPositionDown())
        //{
        //    return FitInTheHole_EDirection.down;
        //}
        //if(CheckPositionDown() && !CheckPositionDownLeft())
        //{
        //    return FitInTheHole_EDirection.downRight;
        //}
        //if(CheckPositionDownLeft() && !CheckPositionLeft())
        //{
        //    return FitInTheHole_EDirection.right;
        //}
        //if(CheckPositionLeft() && !CheckPositionUpLeft())
        //{
        //    return FitInTheHole_EDirection.upRight;
        //}
        //if(CheckPositionUpLeft() && !CheckPositionUp())
        //{
        //    return FitInTheHole_EDirection.up;
        //}
        //return FitInTheHole_EDirection.none;
    }
}
