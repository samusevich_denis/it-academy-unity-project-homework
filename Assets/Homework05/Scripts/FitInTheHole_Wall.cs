﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FitInTheHole_Wall
{
    private Transform [,] cubes;
    private Transform parent;
    public Transform Parent => parent;

    public FitInTheHole_Wall(int sizeX, int sizeY, GameObject cubePrefab)
    {
        GenerateWall(sizeX, sizeY, cubePrefab);
    }

    private void GenerateWall(int sizeX, int sizeY, GameObject prefab)
    {
        cubes = new Transform[sizeY,sizeX];
        parent = new  GameObject().transform;

        for (int x = 0; x < sizeX; x++)
        {
            for (int y = 0; y < sizeY; y++)
            {
                var obj = Object.Instantiate(prefab, new Vector3(x, y, 0f), Quaternion.identity);
                obj.transform.parent = parent;
                cubes[y,x] = obj.transform;
            }
        }        
        parent.position = new Vector3(0f, 0.5f, 0f);
    }

    public void SetupWall(bool [,] figurePrefab, float position,FitInTheHole_PosXY positionHole)
    {
        parent.transform.position = new Vector3(0f, 0f, position);

        foreach (var cube in cubes)
        {
            cube.gameObject.SetActive(true);
        }
        
        for (int y = 0; y < figurePrefab.GetLength(0); y++)
        {
            for (int x = 0; x< figurePrefab.GetLength(1); x++)
            {
                if (!figurePrefab[y,x])
                {
                    continue;
                }
                cubes[y,x].gameObject.SetActive(false);
            }
        }
        cubes[positionHole.Y,positionHole.X].gameObject.SetActive(false);
    }
}
